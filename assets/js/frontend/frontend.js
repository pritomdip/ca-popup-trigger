/**
 * CA Popup Trigger 
 *
 * Copyright (c) 2019 Pritom Chowdhury Dip
 * Licensed under the GPLv2+ license.
 */

/*jslint browser: true */
/*global jQuery:false */

window.Project = (function (window, document, $, undefined) {
	'use strict';
	var app = {
        init: function () {
			$('.ca-sticky-close').on('click', app.closeStickyArea);
			$('.ca-sidebar-left-trigger').on('click', app.sidebarToggleClassAdd);
			$('.ca-messenger-popup-title').on('click', app.messengerShowHide);
			$('.ca-overlay-close-btn').on('click', app.hideOverlay);
			$('.ca-urgency-close').on('click', app.hideUrgency);
			app.overlayPopup();
			app.urgencyPopup();
			app.widgetPopup();
			
		},
		overlayPopup:function(){
			var appearedTime = $('#ca-delayedPopup').attr('data-appear'),
				hidingTime	 = $('#ca-delayedPopup').attr('data-hide');
			appearedTime     = parseInt(appearedTime, 10);
			hidingTime     	 = parseInt(hidingTime, 10); 

			$('#ca-delayedPopup').delay(appearedTime).fadeIn(400);
			$('#ca-delayedPopup').delay(hidingTime).fadeOut();

		},
		urgencyPopup: function(){

			var firstTriggerApperaring  = $('#first-urgency-trigger').attr('data-appear'),
				firstTriggerHiding 		= $('#first-urgency-trigger').attr('data-hide'),
				secondTriggerApperaring = $('#second-urgency-trigger').attr('data-appear'),
				secondTriggerHiding 	= $('#second-urgency-trigger').attr('data-hide'),
				thirdTriggerApperaring  = $('#third-urgency-trigger').attr('data-appear'),
				thirdTriggerHiding 		= $('#third-urgency-trigger').attr('data-hide');

			$('.first-urgency').delay(firstTriggerApperaring).fadeIn();
			$('.second-urgency').delay(secondTriggerApperaring).fadeIn();
			$('.third-urgency').delay(thirdTriggerApperaring).fadeIn();

			$('.first-urgency').delay(firstTriggerHiding).fadeOut();
			$('.second-urgency').delay(secondTriggerHiding).fadeOut();
			$('.third-urgency').delay(thirdTriggerHiding).fadeOut();
		},
		widgetPopup: function(){

			var firstwidgetApperaring  	= $('#ca-first-widget').attr('data-appear'),
				firstwidgetHiding 		= $('#ca-first-widget').attr('data-hide'),
				secondwidgetApperaring  = $('#ca-second-widget').attr('data-appear'),
				secondwidgetHiding 		= $('#ca-second-widget').attr('data-hide');

			$('.ca-widget-1').delay(firstwidgetApperaring).fadeIn();
			$('.ca-widget-2').delay(secondwidgetApperaring).fadeIn();
			$('.ca-widget-1').delay(firstwidgetHiding).fadeOut();
			$('.ca-widget-2').delay(secondwidgetHiding).fadeOut();

		},
		hideUrgency: function(){
			$(this).parent('.ca-urgency-notification').hide();
		},
		hideOverlay: function(e){
			$('#ca-delayedPopup').hide();
			e.preventDefault();
		},
		closeStickyArea: function(){
			$(this).parents('.ca-sticky-overlay').hide();
		},
		sidebarToggleClassAdd: function(){
			$(this).parent().toggleClass('opened closed');
	        $(this).children('.ca-sidebar-title').children('.ca-title-wrapper').children('.ca-triangle').toggleClass('ca-left ca-right');
		},
		messengerShowHide: function(){
			$(this).siblings('.ca-popup-messenger-content-wrapper').toggleClass('ca-is-active ca-popup-not-active');
		}
    };
	$(document).ready(app.init);

	return app;

})(window, document, jQuery);

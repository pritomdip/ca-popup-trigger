<?php
//function prefix capt

/**
 * Get default popup style settings
 * 
 * @return array
 */

function capt_display_style(){
	$types = array(
		''		      => 'Please Select ...',
        'sticky'      => 'Sticky-bar',
        'sidebar'     => 'Sidebar-offer',
		'list'        => 'Sidebar-list',
        'messenger'   => 'Messenger',
        'overlay'     => 'Overlay',
		'urgency'     => 'Urgency-trigger',
        'widget'      => 'Html-widgets'
	);
	return apply_filters( 'capt_display_style', $types );
}

/**
 * Get meta of a popup post
 *
 * @param $post_id, $meta_key, $defult_value
 *
 * @return string
 */

function capt_get_sticky_meta( $post_id, $key = '', $default = false ) {
	$meta_value = get_post_meta( $post_id, $key, true );

	return empty( $meta_value ) ? $default : $meta_value;
}

/**
 * Get default Sticky shortcode settings
 *
 * @param $post_id
 *
 * @return array
 */
function ca_popup_trigger_sticky_get_settings( $post_id ) {
    $default = array(
        'post_id'                   => $post_id,
        'ca_select_style'           => '',
        'ca_sticky_title_1'         => 'Text1',
        'ca_sticky_title_2'         => 'Text2',
        'ca_sticky_title_3'         => 'Text3',
        'ca_sticky_bg_color'        => '#31B0D5',
        'ca_sticky_font_color'      => '#fff',
        'ca_sticky_button_link'     => '#',
        'ca_sticky_button_text'     => 'Button',
        'ca_sticky_button_text_clr' => '#fff',
        'ca_sticky_button_bg_clr'   => '#ddd',
        'ca_sticky_button_hover_clr'=> '#dd3333',
        'ca_sticky_visibility'      => 'show',
        'ca_sticky_cross_btn'       => 'show',
        
    );
    // those are fields which will be merged with post meta
    $default_fields = apply_filters( 'ca_popup_trigger_sticky_default_settings', $default );
    $settings       = array();

    if ( $post_id !== null && get_post_status( $post_id ) ) {
        foreach ( $default_fields as $key => $value ) {
            $saved = get_post_meta( $post_id, $key, true );
            if ( !empty( $saved ) ) {
                $settings[ $key ] = $saved;
            } else {
                $settings[ $key ] = $value;
            }
        }
    }

    return $settings;
}


/**
 * Get default Sidebar shortcode settings
 *
 * @param $post_id
 *
 * @return array
 */
function ca_popup_trigger_sidebar_get_settings( $post_id ) {
    $default = array(
        'post_id'                               => $post_id,
        'ca_select_style'                       => '',
        'ca_sidebar_bg_color'                   => '#fff',
        'ca_sidebar_title'                      => 'The Title',
        'ca_sidebar_title_font_color'           => '#9C27B0',
        'ca_sidebar_title_bg_color'             => '#CDDC39',
        'ca_sidebar_btn_link'                   => '#',
        'ca_sidebar_btn_text'                   => 'Check',
        'ca_sidebar_btn_text_color'             => '#60c46b',
        'ca_sidebar_btn_bg_color'               => '#681e32',
        'ca_sidebar_btn_hover_color'            => '#000000',
        'ca_sidebar_content1'                   => '',
        'ca_sidebar_content1_color'             => '#000',
        'ca_sidebar_content2'                   => '',
        'ca_sidebar_content2_color'             => '#78f900',
        'ca_sidebar_content3'                   => '',
        'ca_sidebar_content3_color'             => '#78f900',
        'ca_sidebar_visibility_on_page_load'    => 'show',
        'ca_sidebar_visibility'                 => 'show',
        
    );
    // those are fields which will be merged with post meta
    $default_fields = apply_filters( 'ca_popup_trigger_sidebar_default_settings', $default );
    $settings       = array();

    if ( $post_id !== null && get_post_status( $post_id ) ) {
        foreach ( $default_fields as $key => $value ) {
            $saved = get_post_meta( $post_id, $key, true );
            if ( !empty( $saved ) ) {
                $settings[ $key ] = $saved;
            } else {
                $settings[ $key ] = $value;
            }
        }
    }

    return $settings;
}

/**
 * Get default Sidebar List shortcode settings
 *
 * @param $post_id
 *
 * @return array
 */
function ca_popup_trigger_list_get_settings( $post_id ) {
    $default = array(
        'post_id'                            => $post_id,
        'ca_select_style'                    => '',
        'ca_list_bg_color'                   => '#fff',
        'ca_list_title'                      => 'The Title',
        'ca_list_title_font_color'           => '#9C27B0',
        'ca_list_title_bg_color'             => '#CDDC39',
        'ca_list_btn_link'                   => '#',
        'ca_list_btn_text'                   => 'Check',
        'ca_list_btn_text_color'             => '#60c46b',
        'ca_list_btn_bg_color'               => '#681e32',
        'ca_list_btn_hover_color'            => '#000000',
        'ca_list_content1'                   => 'example1',
        'ca_list_content2'                   => 'example1',
        'ca_list_content3'                   => 'example1',
        'ca_list_font_color'                 => '#CDDC39',
        'ca_list_visibility_on_page_load'    => 'show',
        'ca_list_visibility'                 => 'show',
        
    );
    // those are fields which will be merged with post meta
    $default_fields = apply_filters( 'ca_popup_trigger_list_default_settings', $default );
    $settings       = array();

    if ( $post_id !== null && get_post_status( $post_id ) ) {
        foreach ( $default_fields as $key => $value ) {
            $saved = get_post_meta( $post_id, $key, true );
            if ( !empty( $saved ) ) {
                $settings[ $key ] = $saved;
            } else {
                $settings[ $key ] = $value;
            }
        }
    }

    return $settings;
}

/**
 * Get Position settings
 *
 * @return array
*/
function ca_popup_trigger_get_position_settings(){
    $types = array(
        'center' => 'Center',
        'left'   => 'Left',
        'right'  => 'Right',
    );

    return apply_filters( 'ca_popup_trigger_get_position_settings', $types );
}

/**
 * Get messanger view list settings
 *
 * @return array
*/
function ca_popup_trigger_msngr_view_settings(){
    $types = array(
        'list_view'     => 'List-View',
        'message_box'   => 'Message-Box',
    );

    return apply_filters( 'ca_popup_trigger_msngr_view_settings', $types );
}

/**
 * Get default Messenger shortcode settings
 *
 * @param $post_id
 *
 * @return array
 */
function ca_popup_trigger_messenger_get_settings( $post_id ) {
    $default = array(
        'post_id'                           => $post_id,
        'ca_select_style'                   => '',
        'ca_msg_bg_color'                   => '#fff',
        'ca_msg_title'                      => 'The Title',
        'ca_msg_title_font_color'           => '#9C27B0',
        'ca_msg_title_bg_color'             => '#CDDC39',
        'ca_msg_btn_link'                   => '#',
        'ca_msg_btn_text'                   => 'Check',
        'ca_msg_btn_text_color'             => '#60c46b',
        'ca_msg_btn_bg_color'               => '#681e32',
        'ca_msg_btn_hover_color'            => '#000000',
        'ca_msg_img_visibility'             => 'no',
        'ca_msg_body_heading'               => '',
        'ca_msg_body_heading_position'      => 'center',
        'ca_msg_body_heading_color'         => '#CDDC39',
        'ca_msg_content_style'              => 'list_view',
        'ca_msg_content_one'                => '',
        'ca_msg_content_two'                => '',
        'ca_msg_content_three'              => '',
        'ca_msg_full_msg'                   => '',
        'ca_msg_content_font_color'         => '#fff',
        'ca_msg_content_position'           => 'center',
        'ca_msg_visibility_on_page_load'    => 'show',
        'ca_msg_popup_position'             => 'left',
        'ca_msg_visibility'                 => 'show',
        
    );
    // those are fields which will be merged with post meta
    $default_fields = apply_filters( 'ca_popup_trigger_messenger_get_settings', $default );
    $settings       = array();

    if ( $post_id !== null && get_post_status( $post_id ) ) {
        foreach ( $default_fields as $key => $value ) {
            $saved = get_post_meta( $post_id, $key, true );
            if ( !empty( $saved ) ) {
                $settings[ $key ] = $saved;
            } else {
                $settings[ $key ] = $value;
            }
        }
    }

    return $settings;
}

/**
 * Get image from post type
 *
 * @param $post_id, $meta key
 *
 * @return String
 */

function ca_popup_trigger_get_image( $id, $image_visibility, $meta_key = 'ca_msg_image' ){

    if( empty( $id ) || $image_visibility == 'no' ) {
        return;
    }

    $ca_img_meta_key  = capt_get_sticky_meta( $id, $meta_key );

    if( empty( $ca_img_meta_key ) ){
        return;
    }

    $default_image    = plugins_url('ca-popup-trigger/assets/images/room-101.jpg');
    $image_attributes = wp_get_attachment_image_src( $ca_img_meta_key, array('345', '245') );
    $src              = empty( $image_attributes[0]) ? $default_image : $image_attributes[0];
   
    return $src;
}

/**
 * Get overlay popup fade settings settings
 *
 * @return array
*/

function ca_popup_trigger_overlay_appear_settings(){
    $types = array(
        'fadeInLeft'    => 'Fade Left',
        'fadeInRight'   => 'Fade Right',
        'fadeInUp'      => 'Fade Up',
        'fadeInDown'    => 'Fade Down',
    );

    return apply_filters( 'ca_popup_trigger_overlay_appear_settings', $types );
}

/**
 * Get default Overlay shortcode settings
 *
 * @param $post_id
 *
 * @return array
 */
function ca_popup_trigger_overlay_get_settings( $post_id ) {
    $default = array(
        'post_id'                               => $post_id,
        'ca_select_style'                       => '',
        'ca_overlay_bg_color'                   => '#fff',
        'ca_overlay_title'                      => 'The Title',
        'ca_overlay_title_font_color'           => '#9C27B0',
        'ca_overlay_title_bg_color'             => '#CDDC39',
        'ca_overlay_title_position'             => 'center',
        'ca_overlay_btn_link'                   => '#',
        'ca_overlay_btn_text'                   => 'Check',
        'ca_overlay_btn_text_color'             => '#60c46b',
        'ca_overlay_btn_bg_color'               => '#681e32',
        'ca_overlay_btn_hover_color'            => '#000000',
        'ca_overlay_img_visibility'             => 'no',
        'ca_overlay_body_heading'               => '',
        'ca_overlay_body_heading_position'      => 'center',
        'ca_overlay_body_heading_color'         => '#CDDC39',
        'ca_overlay_content_style'              => 'list_view',
        'ca_overlay_content_one'                => '',
        'ca_overlay_content_two'                => '',
        'ca_overlay_content_three'              => '',
        'ca_overlay_full_msg'                   => '',
        'ca_overlay_content_font_color'         => '#fff',
        'ca_overlay_content_position'           => 'center',
        'ca_overlay_appear_style'               => 'fadeInLeft',
        'ca_overlay_appearing_time'             => '5000',
        'ca_overlay_hiding_time'                => '10000',
        'ca_overlay_popup_position'             => 'center',
        'ca_overlay_cross_btn'                  => 'show',
        'ca_overlay_visibility_on_page_load'    => 'show',
        
    );
    // those are fields which will be merged with post meta
    $default_fields = apply_filters( 'ca_popup_trigger_overlay_get_settings', $default );
    $settings       = array();

    if ( $post_id !== null && get_post_status( $post_id ) ) {
        foreach ( $default_fields as $key => $value ) {
            $saved = get_post_meta( $post_id, $key, true );
            if ( !empty( $saved ) ) {
                $settings[ $key ] = $saved;
            } else {
                $settings[ $key ] = $value;
            }
        }
    }

    return $settings;
}


/**
 * Get default popup Icons settings
 * 
 * @return array
 */

function ca_popup_trigger_urgency_icons(){
    $types = array(
        'dashicons-admin-site'          => 'Globe',
        'dashicons-phone'               => 'Phone',
        'dashicons-dashboard'           => 'Dashboard',
        'dashicons-admin-post'          => 'Admin Post',
        'dashicons-admin-comments'      => 'Admin Comments',
        'dashicons-admin-users'         => 'Admin Users',
        'dashicons-admin-home'          => 'Admin Home',
        'dashicons-welcome-learn-more'  => 'Learn More',
        'dashicons-welcome-view-site'   => 'View Site',
        'dashicons-format-status'       => 'Formet Status',
        'dashicons-format-chat'         => 'Formet Chat',
        'dashicons-format-audio'        => 'Formet Audio',
        'dashicons-image-filter'        => 'Image Filter',
        'dashicons-editor-help'         => 'Editor Help',
        'dashicons-lock'                => 'Lock',
        'dashicons-visibility'          => 'Visibility',
        'dashicons-calendar-alt'        => 'Calendar',
        'dashicons-post-status'         => 'Post Status',
        'dashicons-edit'                => 'Edit',
        'dashicons-sticky'              => 'Sticky',
        'dashicons-hidden'              => 'Hidden',
        'dashicons-arrow-left'          => 'Arrow Left',
        'dashicons-arrow-right'         => 'Arrow Right',
        'dashicons-arrow-left-alt'      => 'Arrow Left Alt',
        'dashicons-arrow-right-alt'     => 'Arrow Right Alt',
        'dashicons-arrow-up-alt'        => 'Arrow Up Alt',
        'dashicons-arrow-down-alt'      => 'Arrow Down Alt',
        'dashicons-leftright'           => 'Left-Right',
        'dashicons-rss'                 => 'RSS',
        'dashicons-email-alt'           => 'Email',
        'dashicons-heart'               => 'Heart',
        'dashicons-universal-access'    => 'Universal Access',
        'dashicons-buddicons-community' => 'Buddicons Community',
        'dashicons-buddicons-groups'    => 'Buddicons Groups',
        'dashicons-info'                => 'Info',
        'dashicons-cart'                => 'Cart',
        'dashicons-tag'                 => 'Tag',
        'dashicons-archive'             => 'Archive',
        'dashicons-yes'                 => 'Yes',
        'dashicons-yes-alt'             => 'Yes Alt',
        'dashicons-no-alt'              => 'No Alt',
        'dashicons-plus'                => 'Plus',
        'dashicons-minus'               => 'Minus',
        'dashicons-marker'              => 'Marker',
        'dashicons-star-filled'         => 'Star Filled',
        'dashicons-star-empty'          => 'Star Empty',
        'dashicons-flag'                => 'Flag',
        'dashicons-warning'             => 'Warning',
        'dashicons-location'            => 'Location',
        'dashicons-location-alt'        => 'Location Alt',
        'dashicons-sheild-alt'          => 'Sheild',
        'dashicons-search'              => 'Search',
        'dashicons-chart-bar'           => 'Chart Bar',
        'dashicons-chart-line'          => 'Chart Line',
        'dashicons-products'            => 'Products',
        'dashicons-awards'              => 'Awards',
        'dashicons-clock'               => 'Clock',
        'dashicons-lightbulb'           => 'Lightbulb',
        'dashicons-smiley'              => 'Smiley',
        'dashicons-thumbs-up'           => 'Thumbs Up',
        'dashicons-phone'               => 'Phone',
        'dashicons-text-page'           => 'Text Page',
        'dashicons-building'            => 'Building',
        'dashicons-businesswoman'       => 'Businesswoman',
    );
    return apply_filters( 'ca_popup_trigger_urgency_icons', $types );
}

/**
 * Get urgency popup Posiitons settings
 * 
 * @return array
 */

function ca_popup_trigger_urgency_popup_positions(){
    $types = array(
        'top-left'       => 'Top-Left',
        'top-right'      => 'Top-Right',
        'bottom-left'    => 'Bottom-Left',
        'bottom-right'   => 'Bottom-Right',
    );
    return apply_filters( 'ca_popup_trigger_urgency_popup_positions', $types );
}

/**
 * Get default Urgency Trigger shortcode settings
 *
 * @param $post_id
 *
 * @return array
 */
function ca_popup_trigger_urgency_get_settings( $post_id ) {
    $default = array(
        'post_id'                            => $post_id,
        'ca_select_style'                    => '',
        'ca_urgency_content1'                => 'Content one',
        'ca_urgency_content1_bg_color'       => '#fff',
        'ca_urgency_content1_font_color'     => '#000',
        'ca_urgency_content1_appearing_time' => '5000',
        'urgency_content1_hiding_time'       => '20000',
        'ca_urgency_content1_icon'           => 'dashicons-phone',
        'ca_urgency_content2_visible'        => 'no',
        'ca_urgency_content2'                => '',
        'ca_urgency_content2_bg_color'       => '#fff',
        'ca_urgency_content2_font_color'     => '#000',
        'ca_urgency_content2_appearing_time' => '10000',
        'urgency_content2_hiding_time'       => '25000',
        'ca_urgency_content2_icon'           => 'dashicons-phone',
        'ca_urgency_content3_visible'        => 'no',
        'ca_urgency_content3'                => '',
        'ca_urgency_content3_bg_color'       => '#fff',
        'ca_urgency_content3_font_color'     => '#000',
        'ca_urgency_content3_appearing_time' => '15000',
        'urgency_content3_hiding_time'       => '30000',
        'ca_urgency_content3_icon'           => 'dashicons-phone',
        'ca_urgency_popup_position'          => 'bottom-left',
        'ca_urgency_cross_btn'               => 'show',
        'ca_urgency_visibility'              => 'show',
    );
    // those are fields which will be merged with post meta
    $default_fields = apply_filters( 'ca_popup_trigger_urgency_get_settings', $default );
    $settings       = array();

    if ( $post_id !== null && get_post_status( $post_id ) ) {
        foreach ( $default_fields as $key => $value ) {
            $saved = get_post_meta( $post_id, $key, true );
            if ( !empty( $saved ) ) {
                $settings[ $key ] = $saved;
            } else {
                $settings[ $key ] = $value;
            }
        }
    }

    return $settings;
}



/**
 * Get default Html Widget shortcode settings
 *
 * @param $post_id
 *
 * @return array
 */
function ca_popup_trigger_widget_get_settings( $post_id ) {
    $default = array(
        'post_id'                            => $post_id,
        'ca_select_style'                    => '',
        'ca_widget'                          => '',
        'ca_widget1_appearing_time'          => '5000',
        'ca_widget1_hiding_time'             => '20000',
        'ca_widget2'                         => '',
        'ca_widget2_appearing_time'          => '10000',
        'ca_widget2_hiding_time'             => '25000',
        'ca_widget_popup_position'           => 'bottom-left',
        'ca_widget_visibility'               => 'show',    
    );
    // those are fields which will be merged with post meta
    $default_fields = apply_filters( 'ca_popup_trigger_widget_get_settings', $default );
    $settings       = array();

    if ( $post_id !== null && get_post_status( $post_id ) ) {
        foreach ( $default_fields as $key => $value ) {
            $saved = get_post_meta( $post_id, $key, true );
            if ( !empty( $saved ) ) {
                $settings[ $key ] = $saved;
            } else {
                $settings[ $key ] = $value;
            }
        }
    }

    return $settings;
}
<?php

namespace pritom\Capopuptrigger;

class Frontend {
	/**
	 * The single instance of the class.
	 *
	 * @var Frontend
	 * @since 1.0.0
	 */
	protected static $init = null;

	/**
	 * Frontend Instance.
	 *
	 * @since 1.0.0
	 * @static
	 * @return Frontend - Main instance.
	 */
	public static function init() {
		if ( is_null( self::$init ) ) {
			self::$init = new self();
			self::$init->setup();
		}

		return self::$init;
	}

	/**
	 * Initialize all frontend related stuff
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function setup() {
		$this->includes();
		$this->init_hooks();
		$this->instance();
	}

	/**
	 * Includes all frontend related files
	 *
	 * @since 1.0.0
	 * @return void
	 */
	private function includes() {
		require_once dirname( __FILE__ ) . '/device-detect.php';
		require_once dirname( __FILE__ ) . '/class-shortcodes.php';
	}

	/**
	 * Register all frontend related hooks
	 *
	 * @since 1.0.0
	 * @return void
	 */
	private function init_hooks() {
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
	}

	/**
	 * Fire off all the instances
	 *
	 * @since 1.0.0
	 */
	protected function instance() {
		new Shortcode();
	}

	/**
	 * Loads all frontend scripts/styles
	 *
	 * @param $hook
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function enqueue_scripts( $hook ) {
		wp_register_style('ca-popup-trigger', CAPT_ASSETS_URL."/css/frontend.css", CAPT_VERSION);
		wp_register_script('notify', CAPT_ASSETS_URL."/js/vendor/notify.js", ['jquery'], CAPT_VERSION,true);
		wp_register_script('ca-popup-trigger', CAPT_ASSETS_URL."/js/frontend/frontend.js", ['jquery', 'notify'], CAPT_VERSION, true);

		wp_localize_script('ca-popup-trigger', 'capt',
		[
			'ajaxurl'       			=> site_url(). '/wp-admin/admin-ajax.php',
			'nonce'         			=> wp_create_nonce('ca-popup-trigger')
		]);		
		
		wp_enqueue_style('ca-popup-trigger');
		wp_enqueue_script('ca-popup-trigger');
	}

}

Frontend::init();

<?php
namespace pritom\Capopuptrigger;

class Install {
    /**
     * Install constructor.
     */
    public function __construct() {
       // add_action( 'init', array( __CLASS__, 'install' ) );
//        add_filter( 'cron_schedules', array( __CLASS__, 'cron_schedules' ) );
    }

    public static function install() {
        if ( get_option( 'ca_popup_trigger_install_date' ) ) {
            return;
        }

        if ( ! is_blog_installed() ) {
            return;
        }

        // Check if we are not already running this routine.
        if ( 'yes' === get_transient( 'ca_popup_trigger_installing' ) ) {
            return;
        }

        //self::create_options();
        //self::create_tables();
        //self::create_cron_jobs();
        
        delete_transient( 'ca_popup_trigger_installing' );
    }

    /**
     * Save option data
     */
    public static function create_options() {
        //save db version
        update_option( 'wpcp_version', CAPT_VERSION );

        //save install date
        update_option( 'ca_popup_trigger_install_date', current_time( 'timestamp' ) );
    }

    private static function create_tables() {
        global $wpdb;
        $collate = '';
        if ( $wpdb->has_cap( 'collation' ) ) {
            if ( ! empty( $wpdb->charset ) ) {
                $collate .= "DEFAULT CHARACTER SET $wpdb->charset";
            }
            if ( ! empty( $wpdb->collate ) ) {
                $collate .= " COLLATE $wpdb->collate";
            }
        }
        $table_schema = [
            "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}ca_popup_trigger` (
                `faker_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `faker_name` varchar(255) NOT NULL,
                `faker_city` varchar(255) NOT NULL,
                `product_name` varchar(255) NOT NULL,
                `product_image` varchar(255) NOT NULL,
                PRIMARY KEY (`faker_id`)
            ) $collate;",
        ];
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        foreach ( $table_schema as $table ) {
            dbDelta( $table );
        }
    }

    /**
     * Add more cron schedules.
     *
     * @param  array $schedules List of WP scheduled cron jobs.
     *
     * @return array
     */
    public static function cron_schedules( $schedules ) {
        $schedules['monthly'] = array(
            'interval' => 2635200,
            'display'  => __( 'Monthly', 'ca-popup-trigger' ),
        );

        return $schedules;
    }

    /**
     * Create cron jobs (clear them first).
     */
    private static function create_cron_jobs() {
        wp_clear_scheduled_hook( 'ca_popup_trigger_daily_cron' );
        wp_schedule_event( time(), 'daily', 'ca_popup_trigger_daily_cron' );
    }


}

new Install();

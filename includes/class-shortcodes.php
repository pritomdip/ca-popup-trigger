<?php

namespace pritom\Capopuptrigger;

class Shortcode {
    /**
     * Shortcode constructor.
    */
    public function __construct() {
        add_shortcode( 'ca_popup_trigger', array( $this, 'render_stickybar_shortcode' ) );
    }

    /**
     * Render Shortcode.
     *  @since 1.0.0
	 * @return string
    */

    public function render_stickybar_shortcode($attr){

    	$params = shortcode_atts( [ 'id' => null, 'option' => '' ], $attr );

        if ( empty( $params['id'] ) ) {
            return false;
        }
        if( empty( $params['option'] ) ){
        	return false;
        }

        $device_detect = new Capt_Device_Detect;

        if ( $device_detect->isMobile() || $device_detect->isTablet() ) {
			return false;
		}

        $post_id         = $params['id'];
        $selected_option = capt_get_sticky_meta( $post_id, 'ca_select_style' );

        if( $params['option'] != $selected_option ){
        	return;
        }

        switch ( $params['option'] ) {
        	case 'sticky':
        		$settings       = ca_popup_trigger_sticky_get_settings( $post_id );

        		if( $settings['ca_sticky_visibility'] === 'hide' ){
		    		return;
		    	}

		    	ob_start();	

		    	include CAPT_TEMPLATES_DIR . '/frontend/sticky.php';    	
		    	
        		break;

        	case 'sidebar' :

        		$settings       = ca_popup_trigger_sidebar_get_settings( $post_id );

        		if( $settings['ca_sidebar_visibility'] === 'hide' ){
		    		return;
		    	}

		    	if( $settings['ca_sidebar_visibility_on_page_load'] === 'show' ){
		    		$class = 'opened';
		    		$icon  = 'ca-right';
		    	} else {
		    		$class = 'closed';
		    		$icon  = 'ca-left';
		    	}

        		ob_start();

        		include CAPT_TEMPLATES_DIR . '/frontend/sidebar.php';
        	
        		break;

        	case 'list' :

        		$settings       = ca_popup_trigger_list_get_settings( $post_id );

        		if( $settings['ca_list_visibility'] === 'hide' ){
		    		return;
		    	}

		    	if( $settings['ca_list_visibility_on_page_load'] === 'show' ){
		    		$class = 'opened';
		    		$icon  = 'ca-left';
		    	} else {
		    		$class = 'closed';
		    		$icon  = 'ca-right';
		    	}

		    	ob_start();

		    	include CAPT_TEMPLATES_DIR . '/frontend/sidebar-list.php';

        		break;

            case 'messenger' :

                $settings   = ca_popup_trigger_messenger_get_settings( $post_id );

                if( $settings['ca_msg_visibility'] === 'hide' ){
                   return;
                }

                if( $settings['ca_msg_visibility_on_page_load'] === 'show' ){
                    $visibility = 'ca-is-active';
                } else { 
                    $visibility = 'ca-popup-not-active'; 
                }

                if( $settings['ca_msg_popup_position'] === 'left' ){
                    $class = 'ca-popup-left';
                } else { 
                    $class = 'ca-popup-right'; 
                }

                ob_start();

                include CAPT_TEMPLATES_DIR . '/frontend/messenger.php';

                break;

            case 'overlay' :

                $settings       = ca_popup_trigger_overlay_get_settings( $post_id );

                if( $settings['ca_overlay_visibility_on_page_load'] === 'hide' ){
                    return;
                }

                ob_start();

                include CAPT_TEMPLATES_DIR . '/frontend/overlay.php';

                break;

            case 'urgency' :

                $settings       = ca_popup_trigger_urgency_get_settings( $post_id );

                if( $settings['ca_urgency_visibility'] === 'hide' ){
                    return;
                }

                include CAPT_TEMPLATES_DIR . '/frontend/urgency.php';

                break;

            case 'widget' :

               $settings       = ca_popup_trigger_widget_get_settings( $post_id );

                if( $settings['ca_widget_visibility'] === 'hide' ){
                    return;
                }

                ob_start();

                include CAPT_TEMPLATES_DIR . '/frontend/html-widget.php';

                break;
        	
        	default:
        		
        		break;

        	do_action( 'ca_popup_trigger_after_html_content', $settings, $post_id );

        	$html = ob_get_clean();
		    
		    return $html;
        }  
    } 
}
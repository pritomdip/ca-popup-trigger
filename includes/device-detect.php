<?php
namespace pritom\Capopuptrigger;

class Capt_Device_Detect {

	const DETECTION_TYPE_MOBILE = 'mobile';

	const DETECTION_TYPE_EXTENDED = 'extended';

	const VER = '([\w._\+]+)';

	const MOBILE_GRADE_A = 'A';

	const MOBILE_GRADE_B = 'B';

	const MOBILE_GRADE_C = 'C';

	const VERSION = '2.8.22';

	const VERSION_TYPE_STRING = 'text';

	const VERSION_TYPE_FLOAT = 'float';

	protected $cache = array();

	protected $userAgent = null;

	protected $httpHeaders = array();

	protected $cloudfrontHeaders = array();

	protected $matchingRegex = null;

	protected $matchesArray = null;

	protected $detectionType = self::DETECTION_TYPE_MOBILE;

	
	protected static $mobileHeaders = array(

		'HTTP_ACCEPT'                  => array(
			'matches' => array(
				// Opera Mini; @reference: http://dev.opera.com/articles/view/opera-binary-markup-language/
				'application/x-obml2d',
				// BlackBerry devices.
				'application/vnd.rim.html',
				'text/vnd.wap.wml',
				'application/vnd.wap.xhtml+xml'
			)
		),
		'HTTP_X_WAP_PROFILE'           => null,
		'HTTP_X_WAP_CLIENTID'          => null,
		'HTTP_WAP_CONNECTION'          => null,
		'HTTP_PROFILE'                 => null,
		// Reported by Opera on Nokia devices (eg. C3).
		'HTTP_X_OPERAMINI_PHONE_UA'    => null,
		'HTTP_X_NOKIA_GATEWAY_ID'      => null,
		'HTTP_X_ORANGE_ID'             => null,
		'HTTP_X_VODAFONE_3GPDPCONTEXT' => null,
		'HTTP_X_HUAWEI_USERID'         => null,
		// Reported by Windows Smartphones.
		'HTTP_UA_OS'                   => null,
		// Reported by Verizon, Vodafone proxy system.
		'HTTP_X_MOBILE_GATEWAY'        => null,
		// Seen this on HTC Sensation. SensationXE_Beats_Z715e.
		'HTTP_X_ATT_DEVICEID'          => null,
		// Seen this on a HTC.
		'HTTP_UA_CPU'                  => array( 'matches' => array( 'ARM' ) ),
	);

	/**
	 * List of mobile devices (phones).
	 *
	 * @var array
	 */
	protected static $phoneDevices = array(
		'iPhone'       => '\biPhone\b|\biPod\b', // |\biTunes
		'BlackBerry'   => 'BlackBerry|\bBB10\b|rim[0-9]+'
	);

	/**
	 * List of tablet devices.
	 *
	 * @var array
	 */
	protected static $tabletDevices = array(
		// @todo: check for mobile friendly emails topic.
		'iPad'              => 'iPad|iPad.*Mobile',
		// Removed |^.*Android.*Nexus(?!(?:Mobile).)*$
		// @see #442
		'NexusTablet'       => 'Android.*Nexus[\s]+(7|9|10)'
	);

	/**
	 * List of mobile Operating Systems.
	 *
	 * @var array
	 */
	protected static $operatingSystems = array(
		'AndroidOS'       => 'Android',
		'BlackBerryOS'    => 'blackberry|\bBB10\b|rim tablet os'
	);

	/**
	 * List of mobile User Agents.
	 *
	 * @var array
	 */
	protected static $browsers = array(
		// @reference: https://developers.google.com/chrome/mobile/docs/user-agent
		'Chrome'         => '\bCrMo\b|CriOS|Android.*Chrome/[.0-9]* (Mobile)?',
		'Dolfin'         => '\bDolfin\b'
	);

	/**
	 * Utilities.
	 *
	 * @var array
	 */
	protected static $utilities = array(
		// Experimental. When a mobile device wants to switch to 'Desktop Mode'.
		// http://scottcate.com/technology/windows-phone-8-ie10-desktop-or-mobile/
		// https://github.com/serbanghita/Mobile-Detect/issues/57#issuecomment-15024011
		// https://developers.facebook.com/docs/sharing/best-practices
		'Bot'         => 'Googlebot|facebookexternalhit|AdsBot-Google|Google Keyword Suggestion|Facebot|YandexBot|bingbot|ia_archiver|AhrefsBot|Ezooms|GSLFbot|WBSearchBot|Twitterbot|TweetmemeBot|Twikle|PaperLiBot|Wotbox|UnwindFetchor|Exabot|MJ12bot|YandexImages|TurnitinBot|Pingdom',
		'MobileBot'   => 'Googlebot-Mobile|AdsBot-Google-Mobile|YahooSeeker/M1A1-R2D2',
		'DesktopMode' => 'WPDesktop',
		'TV'          => 'SonyDTV|HbbTV', // experimental
		'WebKit'      => '(webkit)[ /]([\w.]+)',
		// @todo: Include JXD consoles.
		'Console'     => '\b(Nintendo|Nintendo WiiU|Nintendo 3DS|PLAYSTATION|Xbox)\b',
		'Watch'       => 'SM-V700',
	);

	/**
	 * All possible HTTP headers that represent the
	 * User-Agent string.
	 *
	 * @var array
	 */
	protected static $uaHttpHeaders = array(
		// The default User-Agent string.
		'HTTP_USER_AGENT',
		// Header can occur on devices using Opera Mini.
		'HTTP_X_OPERAMINI_PHONE_UA',
		// Vodafone specific header: http://www.seoprinciple.com/mobile-web-community-still-angry-at-vodafone/24/
		'HTTP_X_DEVICE_USER_AGENT',
		'HTTP_X_ORIGINAL_USER_AGENT',
		'HTTP_X_SKYFIRE_PHONE',
		'HTTP_X_BOLT_PHONE_UA',
		'HTTP_DEVICE_STOCK_UA',
		'HTTP_X_UCBROWSER_DEVICE_UA'
	);

	/**
	 * The individual segments that could exist in a User-Agent string. VER refers to the regular
	 * expression defined in the constant self::VER.
	 *
	 * @var array
	 */
	protected static $properties = array(

		// Build
		'Mobile'           => 'Mobile/[VER]',
		'Build'            => 'Build/[VER]',
		'Version'          => 'Version/[VER]',
		'VendorID'         => 'VendorID/[VER]',

		// Devices
		'iPad'             => 'iPad.*CPU[a-z ]+[VER]',
		'iPhone'           => 'iPhone.*CPU[a-z ]+[VER]',
		'iPod'             => 'iPod.*CPU[a-z ]+[VER]',
		//'BlackBerry'    => array('BlackBerry[VER]', 'BlackBerry [VER];'),
		'Kindle'           => 'Kindle/[VER]',

		// Browser
		'Chrome'           => array( 'Chrome/[VER]', 'CriOS/[VER]', 'CrMo/[VER]' ),
		'Coast'            => array( 'Coast/[VER]' ),
		'Dolfin'           => 'Dolfin/[VER]',
		// @reference: https://developer.mozilla.org/en-US/docs/User_Agent_Strings_Reference
		'Firefox'          => 'Firefox/[VER]',
		'Fennec'           => 'Fennec/[VER]',
		// http://msdn.microsoft.com/en-us/library/ms537503(v=vs.85).aspx
		// https://msdn.microsoft.com/en-us/library/ie/hh869301(v=vs.85).aspx
		'IE'               => array( 'IEMobile/[VER];', 'IEMobile [VER]', 'MSIE [VER];', 'Trident/[0-9.]+;.*rv:[VER]' ),
		// http://en.wikipedia.org/wiki/NetFront
		'NetFront'         => 'NetFront/[VER]',
		'NokiaBrowser'     => 'NokiaBrowser/[VER]',
		'Opera'            => array( ' OPR/[VER]', 'Opera Mini/[VER]', 'Version/[VER]' ),
		'Opera Mini'       => 'Opera Mini/[VER]',
		'Opera Mobi'       => 'Version/[VER]',
		'UC Browser'       => 'UC Browser[VER]',
		'MQQBrowser'       => 'MQQBrowser/[VER]',
		'MicroMessenger'   => 'MicroMessenger/[VER]',
		'baiduboxapp'      => 'baiduboxapp/[VER]',
		'baidubrowser'     => 'baidubrowser/[VER]',
		'Iron'             => 'Iron/[VER]',
		// @note: Safari 7534.48.3 is actually Version 5.1.
		// @note: On BlackBerry the Version is overwriten by the OS.
		'Safari'           => array( 'Version/[VER]', 'Safari/[VER]' ),
		'Skyfire'          => 'Skyfire/[VER]',
		'Tizen'            => 'Tizen/[VER]',
		'Webkit'           => 'webkit[ /][VER]',
		'PaleMoon'         => 'PaleMoon/[VER]',

		// Engine
		'Gecko'            => 'Gecko/[VER]',
		'Trident'          => 'Trident/[VER]',
		'Presto'           => 'Presto/[VER]',
		'Goanna'           => 'Goanna/[VER]',

		// OS
		'iOS'              => ' \bi?OS\b [VER][ ;]{1}',
		'Android'          => 'Android [VER]',
		'BlackBerry'       => array( 'BlackBerry[\w]+/[VER]', 'BlackBerry.*Version/[VER]', 'Version/[VER]' ),
		'BREW'             => 'BREW [VER]',
		'Java'             => 'Java/[VER]',
		// @reference: http://windowsteamblog.com/windows_phone/b/wpdev/archive/2011/08/29/introducing-the-ie9-on-windows-phone-mango-user-agent-string.aspx
		// @reference: http://en.wikipedia.org/wiki/Windows_NT#Releases
		'Windows Phone OS' => array( 'Windows Phone OS [VER]', 'Windows Phone [VER]' ),
		'Windows Phone'    => 'Windows Phone [VER]',
		'Windows CE'       => 'Windows CE/[VER]',
		// http://social.msdn.microsoft.com/Forums/en-US/windowsdeveloperpreviewgeneral/thread/6be392da-4d2f-41b4-8354-8dcee20c85cd
		'Windows NT'       => 'Windows NT [VER]',
		'Symbian'          => array( 'SymbianOS/[VER]', 'Symbian/[VER]' ),
		'webOS'            => array( 'webOS/[VER]', 'hpwOS/[VER];' ),
	);

	public function __construct(
		array $headers = null,
		$userAgent = null
	) {
		$this->setHttpHeaders( $headers );
		$this->setUserAgent( $userAgent );
	}

	public static function getScriptVersion() {
		return self::VERSION;
	}

	public function setHttpHeaders( $httpHeaders = null ) {
		// use global _SERVER if $httpHeaders aren't defined
		if ( ! is_array( $httpHeaders ) || ! count( $httpHeaders ) ) {
			$httpHeaders = $_SERVER;
		}

		// clear existing headers
		$this->httpHeaders = array();

		// Only save HTTP headers. In PHP land, that means only _SERVER vars that
		// start with HTTP_.
		foreach ( $httpHeaders as $key => $value ) {
			if ( substr( $key, 0, 5 ) === 'HTTP_' ) {
				$this->httpHeaders[$key] = $value;
			}
		}

		// In case we're dealing with CloudFront, we need to know.
		$this->setCfHeaders( $httpHeaders );
	}

	public function getHttpHeaders() {
		return $this->httpHeaders;
	}

	public function getHttpHeader( $header ) {
		// are we using PHP-flavored headers?
		if ( strpos( $header, '_' ) === false ) {
			$header = str_replace( '-', '_', $header );
			$header = strtoupper( $header );
		}

		// test the alternate, too
		$altHeader = 'HTTP_' . $header;

		//Test both the regular and the HTTP_ prefix
		if ( isset( $this->httpHeaders[$header] ) ) {
			return $this->httpHeaders[$header];
		} elseif ( isset( $this->httpHeaders[$altHeader] ) ) {
			return $this->httpHeaders[$altHeader];
		}

		return null;
	}

	public function getMobileHeaders() {
		return self::$mobileHeaders;
	}

	public function getUaHttpHeaders() {
		return self::$uaHttpHeaders;
	}

	public function setCfHeaders( $cfHeaders = null ) {
		// use global _SERVER if $cfHeaders aren't defined
		if ( ! is_array( $cfHeaders ) || ! count( $cfHeaders ) ) {
			$cfHeaders = $_SERVER;
		}

		// clear existing headers
		$this->cloudfrontHeaders = array();

		// Only save CLOUDFRONT headers. In PHP land, that means only _SERVER vars that
		// start with cloudfront-.
		$response = false;
		foreach ( $cfHeaders as $key => $value ) {
			if ( substr( strtolower( $key ), 0, 16 ) === 'http_cloudfront_' ) {
				$this->cloudfrontHeaders[strtoupper( $key )] = $value;
				$response                                    = true;
			}
		}

		return $response;
	}

	public function getCfHeaders() {
		return $this->cloudfrontHeaders;
	}

	public function setUserAgent( $userAgent = null ) {
		// Invalidate cache due to #375
		$this->cache = array();

		if ( false === empty( $userAgent ) ) {
			return $this->userAgent = $userAgent;
		} else {
			$this->userAgent = null;
			foreach ( $this->getUaHttpHeaders() as $altHeader ) {
				if ( false === empty( $this->httpHeaders[$altHeader] ) ) { // @todo: should use getHttpHeader(), but it would be slow. (Serban)
					$this->userAgent .= $this->httpHeaders[$altHeader] . " ";
				}
			}

			if ( ! empty( $this->userAgent ) ) {
				return $this->userAgent = trim( $this->userAgent );
			}
		}

		if ( count( $this->getCfHeaders() ) > 0 ) {
			return $this->userAgent = 'Amazon CloudFront';
		}

		return $this->userAgent = null;
	}

	public function getUserAgent() {
		return $this->userAgent;
	}

	public function setDetectionType( $type = null ) {
		if ( $type === null ) {
			$type = self::DETECTION_TYPE_MOBILE;
		}

		if ( $type !== self::DETECTION_TYPE_MOBILE && $type !== self::DETECTION_TYPE_EXTENDED ) {
			return;
		}

		$this->detectionType = $type;
	}

	public function getMatchingRegex() {
		return $this->matchingRegex;
	}

	public function getMatchesArray() {
		return $this->matchesArray;
	}

	public static function getPhoneDevices() {
		return self::$phoneDevices;
	}

	public static function getTabletDevices() {
		return self::$tabletDevices;
	}

	public static function getUserAgents() {
		return self::getBrowsers();
	}

	public static function getBrowsers() {
		return self::$browsers;
	}

	public static function getUtilities() {
		return self::$utilities;
	}

	public static function getMobileDetectionRules() {
		static $rules;

		if ( ! $rules ) {
			$rules = array_merge(
				self::$phoneDevices,
				self::$tabletDevices,
				self::$operatingSystems,
				self::$browsers
			);
		}

		return $rules;

	}

	public function getMobileDetectionRulesExtended() {
		static $rules;

		if ( ! $rules ) {
			// Merge all rules together.
			$rules = array_merge(
				self::$phoneDevices,
				self::$tabletDevices,
				self::$operatingSystems,
				self::$browsers,
				self::$utilities
			);
		}

		return $rules;
	}

	public function getRules() {
		if ( $this->detectionType == self::DETECTION_TYPE_EXTENDED ) {
			return self::getMobileDetectionRulesExtended();
		} else {
			return self::getMobileDetectionRules();
		}
	}

	public static function getOperatingSystems() {
		return self::$operatingSystems;
	}

	public function checkHttpHeadersForMobile() {

		foreach ( $this->getMobileHeaders() as $mobileHeader => $matchType ) {
			if ( isset( $this->httpHeaders[$mobileHeader] ) ) {
				if ( is_array( $matchType['matches'] ) ) {
					foreach ( $matchType['matches'] as $_match ) {
						if ( strpos( $this->httpHeaders[$mobileHeader], $_match ) !== false ) {
							return true;
						}
					}

					return false;
				} else {
					return true;
				}
			}
		}

		return false;

	}

	public function __call( $name, $arguments ) {
		// make sure the name starts with 'is', otherwise
		if ( substr( $name, 0, 2 ) !== 'is' ) {
			throw new BadMethodCallException( "No such method exists: $name" );
		}

		$this->setDetectionType( self::DETECTION_TYPE_MOBILE );

		$key = substr( $name, 2 );

		return $this->matchUAAgainstKey( $key );
	}

	protected function matchDetectionRulesAgainstUA( $userAgent = null ) {
		// Begin general search.
		foreach ( $this->getRules() as $_regex ) {
			if ( empty( $_regex ) ) {
				continue;
			}

			if ( $this->match( $_regex, $userAgent ) ) {
				return true;
			}
		}

		return false;
	}

	protected function matchUAAgainstKey( $key ) {
		// Make the keys lowercase so we can match: isIphone(), isiPhone(), isiphone(), etc.
		$key = strtolower( $key );
		if ( false === isset( $this->cache[$key] ) ) {

			// change the keys to lower case
			$_rules = array_change_key_case( $this->getRules() );

			if ( false === empty( $_rules[$key] ) ) {
				$this->cache[$key] = $this->match( $_rules[$key] );
			}

			if ( false === isset( $this->cache[$key] ) ) {
				$this->cache[$key] = false;
			}
		}

		return $this->cache[$key];
	}

	public function isMobile( $userAgent = null, $httpHeaders = null ) {

		if ( $httpHeaders ) {
			$this->setHttpHeaders( $httpHeaders );
		}

		if ( $userAgent ) {
			$this->setUserAgent( $userAgent );
		}

		// Check specifically for cloudfront headers if the useragent === 'Amazon CloudFront'
		if ( $this->getUserAgent() === 'Amazon CloudFront' ) {
			$cfHeaders = $this->getCfHeaders();
			if ( array_key_exists( 'HTTP_CLOUDFRONT_IS_MOBILE_VIEWER', $cfHeaders ) && $cfHeaders['HTTP_CLOUDFRONT_IS_MOBILE_VIEWER'] === 'true' ) {
				return true;
			}
		}

		$this->setDetectionType( self::DETECTION_TYPE_MOBILE );

		if ( $this->checkHttpHeadersForMobile() ) {
			return true;
		} else {
			return $this->matchDetectionRulesAgainstUA();
		}

	}

	public function isTablet( $userAgent = null, $httpHeaders = null ) {
		// Check specifically for cloudfront headers if the useragent === 'Amazon CloudFront'
		if ( $this->getUserAgent() === 'Amazon CloudFront' ) {
			$cfHeaders = $this->getCfHeaders();
			if ( array_key_exists( 'HTTP_CLOUDFRONT_IS_TABLET_VIEWER', $cfHeaders ) && $cfHeaders['HTTP_CLOUDFRONT_IS_TABLET_VIEWER'] === 'true' ) {
				return true;
			}
		}

		$this->setDetectionType( self::DETECTION_TYPE_MOBILE );

		foreach ( self::$tabletDevices as $_regex ) {
			if ( $this->match( $_regex, $userAgent ) ) {
				return true;
			}
		}

		return false;
	}

	public function is( $key, $userAgent = null, $httpHeaders = null ) {
		// Set the UA and HTTP headers only if needed (eg. batch mode).
		if ( $httpHeaders ) {
			$this->setHttpHeaders( $httpHeaders );
		}

		if ( $userAgent ) {
			$this->setUserAgent( $userAgent );
		}

		$this->setDetectionType( self::DETECTION_TYPE_EXTENDED );

		return $this->matchUAAgainstKey( $key );
	}

	public function match( $regex, $userAgent = null ) {
		$match = (bool) preg_match( sprintf( '#%s#is', $regex ), ( false === empty( $userAgent ) ? $userAgent : $this->userAgent ), $matches );
		// If positive match is found, store the results for debug.
		if ( $match ) {
			$this->matchingRegex = $regex;
			$this->matchesArray  = $matches;
		}

		return $match;
	}

	public function prepareVersionNo( $ver ) {
		$ver    = str_replace( array( '_', ' ', '/' ), '.', $ver );
		$arrVer = explode( '.', $ver, 2 );

		if ( isset( $arrVer[1] ) ) {
			$arrVer[1] = @str_replace( '.', '', $arrVer[1] ); // @todo: treat strings versions.
		}

		return (float) implode( '.', $arrVer );
	}

	public function version( $propertyName, $type = self::VERSION_TYPE_STRING ) {
		if ( empty( $propertyName ) ) {
			return false;
		}

		// set the $type to the default if we don't recognize the type
		if ( $type !== self::VERSION_TYPE_STRING && $type !== self::VERSION_TYPE_FLOAT ) {
			$type = self::VERSION_TYPE_STRING;
		}

		$properties = self::getProperties();

		// Check if the property exists in the properties array.
		if ( true === isset( $properties[$propertyName] ) ) {

			// Prepare the pattern to be matched.
			// Make sure we always deal with an array (string is converted).
			$properties[$propertyName] = (array) $properties[$propertyName];

			foreach ( $properties[$propertyName] as $propertyMatchString ) {

				$propertyPattern = str_replace( '[VER]', self::VER, $propertyMatchString );

				// Identify and extract the version.
				preg_match( sprintf( '#%s#is', $propertyPattern ), $this->userAgent, $match );

				if ( false === empty( $match[1] ) ) {
					$version = ( $type == self::VERSION_TYPE_FLOAT ? $this->prepareVersionNo( $match[1] ) : $match[1] );

					return $version;
				}

			}

		}

		return false;
	}

	public function mobileGrade() {
		$isMobile = $this->isMobile();

		if (
			// Apple iOS 4-7.0 – Tested on the original iPad (4.3 / 5.0), iPad 2 (4.3 / 5.1 / 6.1), iPad 3 (5.1 / 6.0), iPad Mini (6.1), iPad Retina (7.0), iPhone 3GS (4.3), iPhone 4 (4.3 / 5.1), iPhone 4S (5.1 / 6.0), iPhone 5 (6.0), and iPhone 5S (7.0)
			$this->is( 'iOS' ) && $this->version( 'iPad', self::VERSION_TYPE_FLOAT ) >= 4.3 ||
			$this->is( 'iOS' ) && $this->version( 'iPhone', self::VERSION_TYPE_FLOAT ) >= 4.3 ||
			$this->is( 'iOS' ) && $this->version( 'iPod', self::VERSION_TYPE_FLOAT ) >= 4.3 ||

			( $this->version( 'Android', self::VERSION_TYPE_FLOAT ) > 2.1 && $this->is( 'Webkit' ) ) ||

			$this->version( 'Windows Phone OS', self::VERSION_TYPE_FLOAT ) >= 7.5 ||

			// Tested on the Torch 9800 (6) and Style 9670 (6), BlackBerry® Torch 9810 (7), BlackBerry Z10 (10)
			$this->is( 'BlackBerry' ) && $this->version( 'BlackBerry', self::VERSION_TYPE_FLOAT ) >= 6.0 ||
			// Blackberry Playbook (1.0-2.0) - Tested on PlayBook
			$this->match( 'Playbook.*Tablet' ) ||

			// Palm WebOS (1.4-3.0) - Tested on the Palm Pixi (1.4), Pre (1.4), Pre 2 (2.0), HP TouchPad (3.0)
			( $this->version( 'webOS', self::VERSION_TYPE_FLOAT ) >= 1.4 && $this->match( 'Palm|Pre|Pixi' ) ) ||
			// Palm WebOS 3.0  - Tested on HP TouchPad
			$this->match( 'hp.*TouchPad' ) ||

			// Firefox Mobile 18 - Tested on Android 2.3 and 4.1 devices
			( $this->is( 'Firefox' ) && $this->version( 'Firefox', self::VERSION_TYPE_FLOAT ) >= 18 ) ||

			// Chrome for Android - Tested on Android 4.0, 4.1 device
			( $this->is( 'Chrome' ) && $this->is( 'AndroidOS' ) && $this->version( 'Android', self::VERSION_TYPE_FLOAT ) >= 4.0 ) ||

			// Skyfire 4.1 - Tested on Android 2.3 device
			( $this->is( 'Skyfire' ) && $this->version( 'Skyfire', self::VERSION_TYPE_FLOAT ) >= 4.1 && $this->is( 'AndroidOS' ) && $this->version( 'Android', self::VERSION_TYPE_FLOAT ) >= 2.3 ) ||

			// Opera Mobile 11.5-12: Tested on Android 2.3
			( $this->is( 'Opera' ) && $this->version( 'Opera Mobi', self::VERSION_TYPE_FLOAT ) >= 11.5 && $this->is( 'AndroidOS' ) ) ||

			// Meego 1.2 - Tested on Nokia 950 and N9
			$this->is( 'MeeGoOS' ) ||

			// Tizen (pre-release) - Tested on early hardware
			$this->is( 'Tizen' ) ||

			// Samsung Bada 2.0 - Tested on a Samsung Wave 3, Dolphin browser
			// @todo: more tests here!
			$this->is( 'Dolfin' ) && $this->version( 'Bada', self::VERSION_TYPE_FLOAT ) >= 2.0 ||

			// UC Browser - Tested on Android 2.3 device
			( ( $this->is( 'UC Browser' ) || $this->is( 'Dolfin' ) ) && $this->version( 'Android', self::VERSION_TYPE_FLOAT ) >= 2.3 ) ||

			// Kindle 3 and Fire  - Tested on the built-in WebKit browser for each
			( $this->match( 'Kindle Fire' ) ||
				$this->is( 'Kindle' ) && $this->version( 'Kindle', self::VERSION_TYPE_FLOAT ) >= 3.0 ) ||

			// Nook Color 1.4.1 - Tested on original Nook Color, not Nook Tablet
			$this->is( 'AndroidOS' ) && $this->is( 'NookTablet' ) ||

			// Chrome Desktop 16-24 - Tested on OS X 10.7 and Windows 7
			$this->version( 'Chrome', self::VERSION_TYPE_FLOAT ) >= 16 && ! $isMobile ||

			// Safari Desktop 5-6 - Tested on OS X 10.7 and Windows 7
			$this->version( 'Safari', self::VERSION_TYPE_FLOAT ) >= 5.0 && ! $isMobile ||

			// Firefox Desktop 10-18 - Tested on OS X 10.7 and Windows 7
			$this->version( 'Firefox', self::VERSION_TYPE_FLOAT ) >= 10.0 && ! $isMobile ||

			// Internet Explorer 7-9 - Tested on Windows XP, Vista and 7
			$this->version( 'IE', self::VERSION_TYPE_FLOAT ) >= 7.0 && ! $isMobile ||

			// Opera Desktop 10-12 - Tested on OS X 10.7 and Windows 7
			$this->version( 'Opera', self::VERSION_TYPE_FLOAT ) >= 10 && ! $isMobile
		) {
			return self::MOBILE_GRADE_A;
		}

		if (
			$this->is( 'iOS' ) && $this->version( 'iPad', self::VERSION_TYPE_FLOAT ) < 4.3 ||
			$this->is( 'iOS' ) && $this->version( 'iPhone', self::VERSION_TYPE_FLOAT ) < 4.3 ||
			$this->is( 'iOS' ) && $this->version( 'iPod', self::VERSION_TYPE_FLOAT ) < 4.3 ||

			// Blackberry 5.0: Tested on the Storm 2 9550, Bold 9770
			$this->is( 'Blackberry' ) && $this->version( 'BlackBerry', self::VERSION_TYPE_FLOAT ) >= 5 && $this->version( 'BlackBerry', self::VERSION_TYPE_FLOAT ) < 6 ||

			//Opera Mini (5.0-6.5) - Tested on iOS 3.2/4.3 and Android 2.3
			( $this->version( 'Opera Mini', self::VERSION_TYPE_FLOAT ) >= 5.0 && $this->version( 'Opera Mini', self::VERSION_TYPE_FLOAT ) <= 7.0 &&
				( $this->version( 'Android', self::VERSION_TYPE_FLOAT ) >= 2.3 || $this->is( 'iOS' ) ) ) ||

			// Nokia Symbian^3 - Tested on Nokia N8 (Symbian^3), C7 (Symbian^3), also works on N97 (Symbian^1)
			$this->match( 'NokiaN8|NokiaC7|N97.*Series60|Symbian/3' ) ||

			// @todo: report this (tested on Nokia N71)
			$this->version( 'Opera Mobi', self::VERSION_TYPE_FLOAT ) >= 11 && $this->is( 'SymbianOS' )
		) {
			return self::MOBILE_GRADE_B;
		}

		if (
			// Blackberry 4.x - Tested on the Curve 8330
			$this->version( 'BlackBerry', self::VERSION_TYPE_FLOAT ) <= 5.0 ||
			// Windows Mobile - Tested on the HTC Leo (WinMo 5.2)
			$this->match( 'MSIEMobile|Windows CE.*Mobile' ) || $this->version( 'Windows Mobile', self::VERSION_TYPE_FLOAT ) <= 5.2 ||

			// Tested on original iPhone (3.1), iPhone 3 (3.2)
			$this->is( 'iOS' ) && $this->version( 'iPad', self::VERSION_TYPE_FLOAT ) <= 3.2 ||
			$this->is( 'iOS' ) && $this->version( 'iPhone', self::VERSION_TYPE_FLOAT ) <= 3.2 ||
			$this->is( 'iOS' ) && $this->version( 'iPod', self::VERSION_TYPE_FLOAT ) <= 3.2 ||

			// Internet Explorer 7 and older - Tested on Windows XP
			$this->version( 'IE', self::VERSION_TYPE_FLOAT ) <= 7.0 && ! $isMobile
		) {
			return self::MOBILE_GRADE_C;
		}

		// All older smartphone platforms and featurephones - Any device that doesn't support media queries
		// will receive the basic, C grade experience.
		return self::MOBILE_GRADE_C;
	}
}
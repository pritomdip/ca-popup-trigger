<?php

namespace Pritom\Capopuptrigger\Admin;

class PostTypes {
    /**
     * PostTypes constructor.
     */
    public function __construct() {
        add_action( 'init', array( $this, 'register_post_types' ) );
        add_action( 'init', array( $this, 'register_taxonomies' ) );
    }

    /**
     * Register custom post types
     */
    public function register_post_types() {
        register_post_type( 'ca_popup', array(
            'labels'              => $this->get_posts_labels( 'CA Popup', __( 'CA Popup', 'ca-popup-trigger' ), __( 'CA Popup', 'ca-popup-trigger' ) ),
            'hierarchical'        => false,
            'supports'            => array(  'title', 'editor', 'thumbnail' ),
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-admin-tools',
            'publicly_queryable'  => true,
            'exclude_from_search' => false,
            'has_archive'         => true,
            'query_var'           => true,
            'can_export'          => true,
            'rewrite'             => true,
            'capability_type'     => 'post',
        ) );

    }

	/**
	 * Register custom taxonomies
	 *
	 * @since 1.0.0
	 */
    public function register_taxonomies() {
        register_taxonomy( 'ca_popup_category', array( 'ca_popup' ), array(
            'hierarchical'      => true,
            'labels'            => $this->get_posts_labels( 'CA Popup Category', __( 'CA Popup Category', 'ca-popup-trigger' ), __( 'CA Popup Categories', 'ca-popup-trigger' ) ),
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
        ) );

    }

	/**
	 * Get all labels from post types
	 *
	 * @param $menu_name
	 * @param $singular
	 * @param $plural
	 *
	 * @return array
	 * @since 1.0.0
	 */
    protected static function get_posts_labels( $menu_name, $singular, $plural ) {
        $labels = array(
            'name'               => $singular,
            'all_items'          => sprintf( __( "All %s", 'ca-popup-trigger' ), $plural ),
            'singular_name'      => $singular,
            'add_new'            => sprintf( __( 'New %s', 'ca-popup-trigger' ), $singular ),
            'add_new_item'       => sprintf( __( 'Add New %s', 'ca-popup-trigger' ), $singular ),
            'edit_item'          => sprintf( __( 'Edit %s', 'ca-popup-trigger' ), $singular ),
            'new_item'           => sprintf( __( 'New %s', 'ca-popup-trigger' ), $singular ),
            'view_item'          => sprintf( __( 'View %s', 'ca-popup-trigger' ), $singular ),
            'search_items'       => sprintf( __( 'Search %s', 'ca-popup-trigger' ), $plural ),
            'not_found'          => sprintf( __( 'No %s found', 'ca-popup-trigger' ), $plural ),
            'not_found_in_trash' => sprintf( __( 'No %s found in Trash', 'ca-popup-trigger' ), $plural ),
            'parent_item_colon'  => sprintf( __( 'Parent %s:', 'ca-popup-trigger' ), $singular ),
            'menu_name'          => $menu_name,
        );

        return $labels;
    }

	/**
	 * Get all labels from taxonomies
	 *
	 * @param $menu_name
	 * @param $singular
	 * @param $plural
	 *
	 * @return array
	 * @since 1.0.0
	 */
    protected static function get_taxonomy_label( $menu_name, $singular, $plural ) {
        $labels = array(
            'name'              => sprintf( _x( '%s', 'taxonomy general name', 'ca-popup-trigger' ), $plural ),
            'singular_name'     => sprintf( _x( '%s', 'taxonomy singular name', 'ca-popup-trigger' ), $singular ),
            'search_items'      => sprintf( __( 'Search %', 'ca-popup-trigger' ), $plural ),
            'all_items'         => sprintf( __( 'All %s', 'ca-popup-trigger' ), $plural ),
            'parent_item'       => sprintf( __( 'Parent %s', 'ca-popup-trigger' ), $singular ),
            'parent_item_colon' => sprintf( __( 'Parent %s:', 'ca-popup-trigger' ), $singular ),
            'edit_item'         => sprintf( __( 'Edit %s', 'ca-popup-trigger' ), $singular ),
            'update_item'       => sprintf( __( 'Update %s', 'ca-popup-trigger' ), $singular ),
            'add_new_item'      => sprintf( __( 'Add New %s', 'ca-popup-trigger' ), $singular ),
            'new_item_name'     => sprintf( __( 'New % Name', 'ca-popup-trigger' ), $singular ),
            'menu_name'         => __( $menu_name, 'ca-popup-trigger' ),
        );

        return $labels;
    }
}

<?php

namespace Pritom\Capopuptrigger\Admin;

class MetaBox{
	/**
	 * MetaBox constructor.
	 */

	public function __construct(){
		add_action('add_meta_boxes_ca_popup', array( $this, 'register_ca_popup_metabox' ));
		add_action('save_post_ca_popup', array( $this, 'save_ca_popup'), 10, 3 );
	}

	/**
	 * Register Ca Popup metaboxes
	 *
	 */

	public function register_ca_popup_metabox(){
		global $post;

		add_meta_box('ca_popup_settings', __('CA Pop up Settings', 'ca-popup-trigger'), array(
			$this, 'render_popup_settings_metabox' ), 'ca_popup' );

		if( $post->post_status === 'publish' || $post->status === 'draft' ){
			add_meta_box('ca_popup_shortcodes', __('CA Pop up Shortcodes', 'ca-popup-trigger'), array(
				$this, 'render_popup_shortcode_metabox'), 'ca_popup', 'side'
			);
		}
		
	}

	/**
	 * Render Ca Popup meta data
	 *
	 * @param $post_id
	 */

	public function render_popup_settings_metabox($post){
		ob_start();
		include CAPT_TEMPLATES_DIR . '/admin/popup-meta-field.php';
		$html = ob_get_clean();
		echo $html;
	}

	/**
	 * Render Ca Popup shortcode in side
	 *
	 * @param $post
	 */

	public function render_popup_shortcode_metabox( $post ){
		$id 	   = $post->ID;
		$selection = get_post_meta( $id, 'ca_select_style', true );

		if( $selection == '' ){
			return;
		}

		echo "<code>[ca_popup_trigger option='{$selection}' id='{$id}']</code>";
        echo '<p>' . __( 'Use the shortocode to render the popup anywhere in page.', 'ca-popup-trigger' ) . '</p>';
	}

	/**
	 * save Ca Popup meta data
	 *
	 * @param $post_id
	 */

	public function save_ca_popup( $post_id, $post, $update ){

		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return;
		}

		if (defined('DOING_AJAX') && DOING_AJAX) {
			return;
		}
		//Get meta fields value 
		include CAPT_TEMPLATES_DIR . '/admin/get-meta-field.php';

		// Update meta fields value
		include CAPT_TEMPLATES_DIR . '/admin/save-meta-field.php';
	}

}

<?php

namespace Pritom\Capopuptrigger\Admin;
class Admin_Menu{
	/**
	 * Admin Menu constructor.
	 */

	public function __construct(){
		add_action( 'manage_ca_popup_posts_columns', array( $this, 'edit_ca_popup_columns' ) );
		add_action( 'manage_ca_popup_posts_custom_column', array( $this, 'add_shortodes_to_columns' ), 10, 3);
	}

	/**
	 * Adding Columns in admin list table
	 *
	 * @param $column
	 */

	public function edit_ca_popup_columns( $columns ) {
	    unset( $columns['date'] );
	    $columns['status'] = __( 'Status', 'ca-popup-trigger' );
	    $columns['shortcodes'] = __( 'Shortcodes', 'ca-popup-trigger' );
	    $columns['date'] = __( 'Date', 'ca-popup-trigger' );

	    return $columns;
	}

	/**
	 * Column data in admin list table
	 *
	 * @param $column, $post_id
	 */

	public function add_shortodes_to_columns( $column, $post_id ) {
	    switch ( $column ) {
	        case 'shortcodes' :
	        	$selection = get_post_meta( $post_id, 'ca_select_style', true );
	            echo "[ca_popup_trigger option='{$selection}' id='{$post_id}']"; 
	        break;

	        case 'status' :
	        	$selection = get_post_meta( $post_id, 'ca_select_style', true );
	            
	        	switch ( $selection ){
	        		case 'sticky' :
	        			$status = get_post_meta( $post_id, 'ca_sticky_visibility', true );
	        		break;

	        		case 'sidebar' :
	        			$status = get_post_meta( $post_id, 'ca_sidebar_visibility', true );
	        		break;

	        		case 'list' :
	        			$status = get_post_meta( $post_id, 'ca_list_visibility', true );
	        		break;

	        		case 'messenger' :
	        			$status = get_post_meta( $post_id, 'ca_msg_visibility', true );
	        		break;

	        		case 'overlay' :
	        			$status = get_post_meta( $post_id, 'ca_overlay_visibility_on_page_load', true );
	        		break;

	        		case 'urgency' :
	        			$status = get_post_meta( $post_id, 'ca_urgency_visibility', true );
	        		break;

	        		case 'widget' :
	        			$status = get_post_meta( $post_id, 'ca_widget_visibility', true );
	        		break;

	        		default:
	        			$status = '';
	        	}

	        	echo ucfirst($status);

	        break;
	    }
	}
}

<?php
$urgency_content1       	   	 = capt_get_sticky_meta( $post->ID, 'ca_urgency_content1', 'Content one' );
$urgency_content1_bg_color    	 = capt_get_sticky_meta( $post->ID, 'ca_urgency_content1_bg_color','#fff');
$urgency_content1_font_color 	 = capt_get_sticky_meta( $post->ID, 'ca_urgency_content1_font_color', '#000' );
$urgency_content1_appearing_time = capt_get_sticky_meta( $post->ID, 'ca_urgency_content1_appearing_time', '5000' );
$urgency_content1_hiding_time 	 = capt_get_sticky_meta( $post->ID, 'ca_urgency_content1_hiding_time', '20000' );
$urgency_content1_icon 	 		 = capt_get_sticky_meta( $post->ID, 'ca_urgency_content1_icon', 'dashicons-phone' );
$urgency_content2_visible        = capt_get_sticky_meta( $post->ID, 'ca_urgency_content2_visible', 'no' );
$urgency_content2       	   	 = capt_get_sticky_meta( $post->ID, 'ca_urgency_content2', '' );
$urgency_content2_bg_color    	 = capt_get_sticky_meta( $post->ID, 'ca_urgency_content2_bg_color','#fff');
$urgency_content2_font_color 	 = capt_get_sticky_meta( $post->ID, 'ca_urgency_content2_font_color', '#000' );
$urgency_content2_appearing_time = capt_get_sticky_meta( $post->ID, 'ca_urgency_content2_appearing_time', '10000' );
$urgency_content2_hiding_time 	 = capt_get_sticky_meta( $post->ID, 'ca_urgency_content2_hiding_time', '25000' );
$urgency_content2_icon 	 		 = capt_get_sticky_meta( $post->ID, 'ca_urgency_content2_icon', 'dashicons-phone' );
$urgency_content3_visible        = capt_get_sticky_meta( $post->ID, 'ca_urgency_content3_visible', 'no' );
$urgency_content3       	   	 = capt_get_sticky_meta( $post->ID, 'ca_urgency_content3', '' );
$urgency_content3_bg_color    	 = capt_get_sticky_meta( $post->ID, 'ca_urgency_content3_bg_color','#fff');
$urgency_content3_font_color 	 = capt_get_sticky_meta( $post->ID, 'ca_urgency_content3_font_color', '#000' );
$urgency_content3_appearing_time = capt_get_sticky_meta( $post->ID, 'ca_urgency_content3_appearing_time', '15000' );
$urgency_content3_hiding_time 	 = capt_get_sticky_meta( $post->ID, 'ca_urgency_content3_hiding_time', '30000' );
$urgency_content3_icon 	 		 = capt_get_sticky_meta( $post->ID, 'ca_urgency_content3_icon', 'dashicons-phone' );
$urgency_popup_position 	 	 = capt_get_sticky_meta( $post->ID, 'ca_urgency_popup_position', 'bottom-left' );
$urgency_cross_btn 	 	 		 = capt_get_sticky_meta( $post->ID, 'ca_urgency_cross_btn', 'show' );
$urgency_visibility 	 	 	 = capt_get_sticky_meta( $post->ID, 'ca_urgency_visibility', 'show');
?>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_urgency_content1"><?php _e("Content One Message:","ca-popup-trigger"); ?></label>
		<textarea name="ca_urgency_content1" maxlength="100"><?php echo $urgency_content1; ?></textarea>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_urgency_content1_bg_color"><?php _e("Content One Background Color:","ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_urgency_content1_bg_color" value="<?php echo $urgency_content1_bg_color; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_urgency_content1_font_color"><?php _e("Content One Font Color:","ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_urgency_content1_font_color" value="<?php echo $urgency_content1_font_color; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_urgency_content1_appearing_time"><?php _e("Content One Appearing Time: (ms)","ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_urgency_content1_appearing_time" value="<?php echo $urgency_content1_appearing_time; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_urgency_content1_hiding_time"><?php _e("Content One Hide After Time: (ms)","ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_urgency_content1_hiding_time" value="<?php echo $urgency_content1_hiding_time; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_urgency_content1_icon"><?php _e("Content One Icon:","ca-popup-trigger"); ?></label>
		<select name="ca_urgency_content1_icon" id="ca_urgency_content1_icon">

			<?php
			$icons = ca_popup_trigger_urgency_icons();
			
			foreach ( $icons as $key => $value ){ ?>
				<option value="<?php echo $key ?>" <?php echo ( $key == $urgency_content1_icon ) ? 'selected' : '' ?>><?php echo $value; ?></option>
			<?php } ?>

		</select>
		<span class="dashicons ca_content1_icon_placement"></span>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_urgency_content2_visible"><?php _e("Add Second Urgency Trigger:", "ca-popup-trigger"); ?></label> 
		<select name="ca_urgency_content2_visible" id="ca_content2_visibility">
			<option value="no"<?php echo ('no' == $urgency_content2_visible) ? 'selected' : '' ?>>No</option>
			<option value="yes" <?php echo ('yes' == $urgency_content2_visible) ? 'selected' : '' ?>>Yes</option>	
		</select>
	</div>
</div>

<div id="trigger2">

	<div class="ca-row">
		<div class="form-group">
			<label for="ca_urgency_content2"><?php _e("Content Two Message:","ca-popup-trigger"); ?></label>
			<textarea name="ca_urgency_content2" placeholder="content 2" maxlength="100"><?php echo $urgency_content2; ?></textarea>
		</div>
	</div>

	<div class="ca-row">
		<div class="form-group">
			<label for="ca_urgency_content2_bg_color"><?php _e("Content Two Background Color:","ca-popup-trigger"); ?></label>
			<input type="text" class="form-table form-control ca-color-field" name="ca_urgency_content2_bg_color" value="<?php echo $urgency_content2_bg_color; ?>"/>
		</div>
	</div>

	<div class="ca-row">
		<div class="form-group">
			<label for="ca_urgency_content2_font_color"><?php _e("Content Two Font Color:","ca-popup-trigger"); ?></label>
			<input type="text" class="form-table form-control ca-color-field" name="ca_urgency_content2_font_color" value="<?php echo $urgency_content2_font_color; ?>"/>
		</div>
	</div>

	<div class="ca-row">
		<div class="form-group">
			<label for="ca_urgency_content2_appearing_time"><?php _e("Content Two Appearing Time: (ms)","ca-popup-trigger"); ?></label>
			<input type="text" class="form-table form-control" name="ca_urgency_content2_appearing_time" value="<?php echo $urgency_content2_appearing_time; ?>"/>
		</div>
	</div>

	<div class="ca-row">
		<div class="form-group">
			<label for="ca_urgency_content2_hiding_time"><?php _e("Content Two Hide After Time: (ms)","ca-popup-trigger"); ?></label>
			<input type="text" class="form-table form-control" name="ca_urgency_content2_hiding_time" value="<?php echo $urgency_content2_hiding_time; ?>"/>
		</div>
	</div>

	<div class="ca-row">
		<div class="form-group">
			<label for="ca_urgency_content2_icon"><?php _e("Content Two Icon:","ca-popup-trigger"); ?></label>
			<select name="ca_urgency_content2_icon" id="ca_urgency_content2_icon">

				<?php
				$icons = ca_popup_trigger_urgency_icons();
				
				foreach ( $icons as $key => $value ){ ?>
					<option value="<?php echo $key ?>" <?php echo ( $key == $urgency_content2_icon ) ? 'selected' : '' ?>><?php echo $value; ?></option>
				<?php } ?>

			</select>
			<span class="dashicons ca_content2_icon_placement"></span>
		</div>
	</div>

</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_urgency_content2_visible"><?php _e("Add Third Urgency Trigger:", "ca-popup-trigger"); ?></label> 
		<select name="ca_urgency_content3_visible" id="ca_content3_visibility">
			<option value="no"<?php echo ('no' == $urgency_content3_visible) ? 'selected' : '' ?>>No</option>
			<option value="yes" <?php echo ('yes' == $urgency_content3_visible) ? 'selected' : '' ?>>Yes</option>	
		</select>
	</div>
</div>

<div id="trigger3">

	<div class="ca-row">
		<div class="form-group">
			<label for="ca_urgency_content3"><?php _e("Content Three Message:","ca-popup-trigger"); ?></label>
			<textarea name="ca_urgency_content3"  placeholder="content 3" maxlength="100"><?php echo $urgency_content3; ?></textarea>
		</div>
	</div>

	<div class="ca-row">
		<div class="form-group">
			<label for="ca_urgency_content3_bg_color"><?php _e("Content Three Background Color:","ca-popup-trigger"); ?></label>
			<input type="text" class="form-table form-control ca-color-field" name="ca_urgency_content3_bg_color" value="<?php echo $urgency_content3_bg_color; ?>"/>
		</div>
	</div>

	<div class="ca-row">
		<div class="form-group">
			<label for="ca_urgency_content3_font_color"><?php _e("Content Three Font Color:","ca-popup-trigger"); ?></label>
			<input type="text" class="form-table form-control ca-color-field" name="ca_urgency_content3_font_color" value="<?php echo $urgency_content3_font_color; ?>"/>
		</div>
	</div>

	<div class="ca-row">
		<div class="form-group">
			<label for="ca_urgency_content3_appearing_time"><?php _e("Content Three Appearing Time: (ms)","ca-popup-trigger"); ?></label>
			<input type="text" class="form-table form-control" name="ca_urgency_content3_appearing_time" value="<?php echo $urgency_content3_appearing_time; ?>"/>
		</div>
	</div>

	<div class="ca-row">
		<div class="form-group">
			<label for="ca_urgency_content3_hiding_time"><?php _e("Content Three Hide After Time: (ms)","ca-popup-trigger"); ?></label>
			<input type="text" class="form-table form-control" name="ca_urgency_content3_hiding_time" value="<?php echo $urgency_content3_hiding_time; ?>"/>
		</div>
	</div>

	<div class="ca-row">
		<div class="form-group">
			<label for="ca_urgency_content3_icon"><?php _e("Content Three Icon:","ca-popup-trigger"); ?></label>
			<select name="ca_urgency_content3_icon" id="ca_urgency_content3_icon">

				<?php
				$icons = ca_popup_trigger_urgency_icons();
				
				foreach ( $icons as $key => $value ){ ?>
					<option value="<?php echo $key ?>" <?php echo ( $key == $urgency_content3_icon ) ? 'selected' : '' ?>><?php echo $value; ?></option>
				<?php } ?>

			</select>
			<span class="dashicons ca_content3_icon_placement"></span>
		</div>
	</div>

</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_urgency_popup_position"><?php _e("Urgency Popup Position:","ca-popup-trigger"); ?></label>
		<select name="ca_urgency_popup_position">
			<?php 
			$options = ca_popup_trigger_urgency_popup_positions();
			foreach ( $options as $key => $value ){ ?>
				<option value="<?php echo $key ?>" <?php echo ( $key == $urgency_popup_position ) ? 'selected' : '' ?>><?php echo $value; ?></option>
			<?php } ?>
		</select>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_urgency_cross_btn"><?php _e("Cross Button:", "ca-popup-trigger"); ?></label> 
		<select name="ca_urgency_cross_btn">
			<option value="show" <?php echo ('show' == $urgency_cross_btn) ? 'selected' : '' ?>>Yes</option>
			<option value="hide"<?php echo ('hide' == $urgency_cross_btn) ? 'selected' : '' ?>>No</option>
		</select>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_urgency_visibility"><?php _e("Urgency Visibility On Page Load:", "ca-popup-trigger"); ?></label> 
		<select name="ca_urgency_visibility">
			<option value="show" <?php echo ('show' == $urgency_visibility) ? 'selected' : '' ?>>Open</option>
			<option value="hide"<?php echo ('hide' == $urgency_visibility) ? 'selected' : '' ?>>Close</option>
		</select>
	</div>
</div>
<?php 
$overlay_bg_color       		= capt_get_sticky_meta( $post->ID, 'ca_overlay_bg_color', '#fff' );
$overlay_title          		= capt_get_sticky_meta( $post->ID, 'ca_overlay_title', 'The title' );
$overlay_title_font_clr 		= capt_get_sticky_meta( $post->ID, 'ca_overlay_title_font_color', '#9C27B0' );
$overlay_title_bg_clr   		= capt_get_sticky_meta( $post->ID, 'ca_overlay_title_bg_color', '#CDDC39' );
$overlay_title_position   		= capt_get_sticky_meta( $post->ID, 'ca_overlay_title_position', 'center' );
$overlay_btn_link   			= capt_get_sticky_meta( $post->ID, 'ca_overlay_btn_link', '#' );
$overlay_btn_text   			= capt_get_sticky_meta( $post->ID, 'ca_overlay_btn_text', 'Check' );
$overlay_btn_text_clr   		= capt_get_sticky_meta( $post->ID, 'ca_overlay_btn_text_color', '#60c46b' );
$overlay_btn_bg_clr   			= capt_get_sticky_meta( $post->ID, 'ca_overlay_btn_bg_color', '#681e32' );
$overlay_btn_hover_clr 	 		= capt_get_sticky_meta( $post->ID, 'ca_overlay_btn_hover_color', '#000000' );
$overlay_img_visibility      	= capt_get_sticky_meta( $post->ID, 'ca_overlay_img_visibility', 'no' );
$overlay_body_heading      		= capt_get_sticky_meta( $post->ID, 'ca_overlay_body_heading','');
$chosen_style					= capt_get_sticky_meta( $post->ID, 'ca_overlay_body_heading_position', 'center');
$overlay_body_heading_color		= capt_get_sticky_meta( $post->ID, 'ca_overlay_body_heading_color', '#CDDC39');
$overlay_content_style			= capt_get_sticky_meta( $post->ID, 'ca_overlay_content_style', 'list_view');
$overlay_content_one			= capt_get_sticky_meta( $post->ID, 'ca_overlay_content_one', '');
$overlay_content_two			= capt_get_sticky_meta( $post->ID, 'ca_overlay_content_two', '');
$overlay_content_three			= capt_get_sticky_meta( $post->ID, 'ca_overlay_content_three', '');
$overlay_full_msg				= capt_get_sticky_meta( $post->ID, 'ca_overlay_full_msg', '');
$overlay_content_font_color		= capt_get_sticky_meta( $post->ID, 'ca_overlay_content_font_color', '#fff');
$overlay_content_position		= capt_get_sticky_meta( $post->ID, 'ca_overlay_content_position', 'center');
$overlay_appear_style			= capt_get_sticky_meta( $post->ID, 'ca_overlay_appear_style', 'fadeInLeft');
$overlay_appearing_time			= capt_get_sticky_meta( $post->ID, 'ca_overlay_appearing_time', '5000');
$overlay_hiding_time			= capt_get_sticky_meta( $post->ID, 'ca_overlay_hiding_time', '10000');
$overlay_popup_position			= capt_get_sticky_meta( $post->ID, 'ca_overlay_popup_position', 'center');
$overlay_cross_btn				= capt_get_sticky_meta( $post->ID, 'ca_overlay_cross_btn', 'show');
$overlay_visibility_on_load		= capt_get_sticky_meta( $post->ID, 'ca_overlay_visibility_on_page_load', 'show');
?>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_bg_color"><?php _e("Background Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_overlay_bg_color" value="<?php echo $overlay_bg_color; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_title"><?php _e("Title: (Only 30 Characters)", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_overlay_title" value="<?php echo $overlay_title; ?>" maxlength="30"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_title_font_color"><?php _e("Title Font Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_overlay_title_font_color" value="<?php echo $overlay_title_font_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_title_bg_color"><?php _e("Title Background Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_overlay_title_bg_color" value="<?php echo $overlay_title_bg_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_title_position"><?php _e("Overlay Title Position:", "ca-popup-trigger"); ?></label>
		<select name="ca_overlay_title_position">
			<?php 
			$options = ca_popup_trigger_get_position_settings();
			foreach ( $options as $key => $value ){ ?>
				<option value="<?php echo $key ?>" <?php echo ( $key == $overlay_title_position ) ? 'selected' : '' ?>><?php echo $value; ?></option>
			<?php } ?>
		</select>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_btn_link"><?php _e("Button Link:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_overlay_btn_link" value="<?php echo $overlay_btn_link; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_btn_text"><?php _e("Button Text:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_overlay_btn_text" value="<?php echo $overlay_btn_text; ?>" maxlength="11"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_btn_text_color"><?php _e("Button Text Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_overlay_btn_text_color" value="<?php echo $overlay_btn_text_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_btn_bg_color"><?php _e("Button Background Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_overlay_btn_bg_color" value="<?php echo $overlay_btn_bg_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_btn_hover_color"><?php _e("Button Mouse Hover Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_overlay_btn_hover_color" value="<?php echo $overlay_btn_hover_clr; ?>"/>
	</div>
</div>


<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_img_visibility"><?php _e("Add Image:", "ca-popup-trigger"); ?></label> 
		<select name="ca_overlay_img_visibility" id="ca_overlay_img_visibility">
			<option value="no"<?php echo ('no' == $overlay_img_visibility) ? 'selected' : '' ?>>No</option>
			<option value="yes" <?php echo ('yes' == $overlay_img_visibility) ? 'selected' : '' ?>>Yes</option>	
		</select>
	</div>
</div>

<div class="ca-row" id="overlay-img">
	<div class="form-group">

		<label for="ca_overlay_img_upload"><?php _e("Image Upload:", "ca-popup-trigger"); ?></label>

		<div class="upload">
		<?php
		$ca_img_meta_key 	   = capt_get_sticky_meta( $post->ID, 'ca_overlay_image');
		$default_image 		   = plugins_url('ca-popup-trigger/assets/images/room-101.jpg');

		if( !empty( $ca_img_meta_key ) ){
			$image_attributes = wp_get_attachment_image_src( $ca_img_meta_key );
        	$src 			  = empty($image_attributes[0]) ? $default_image : $image_attributes[0];
        	$value 			  = $ca_img_meta_key;
		} else{
			$src 			  = $default_image;
			$value 			  = '';
		}	
		?>
            <img data-src="<?php echo $default_image; ?>" style="display:inline-block;vertical-align: top; margin-left: 20px;" src="<?php echo $src; ?>" width="100px" height="100px" />
            <div style="display:inline-block;vertical-align: top;" >
                <input type="hidden" name="ca_overlay_image" id="ca-overlay-image" value="<?php echo $value; ?>" />
                <button type="submit" class="upload_image_button button"><?php _e( 'Upload', 'ca-popup-trigger' ); ?></button>
                <button type="submit" class="remove_image_button button">&times;</button>
            </div>

        </div>

	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_body_heading"><?php _e("Body Heading:", "ca-popup-trigger"); ?></label>
		<input type="text" placeholder="Heading Goes Here" class="form-table form-control" name="ca_overlay_body_heading" value="<?php echo $overlay_body_heading; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_body_heading_position"><?php _e("Body Heading Position:", "ca-popup-trigger"); ?></label>
		<select name="ca_overlay_body_heading_position">
			<?php 
			$options = ca_popup_trigger_get_position_settings();
			foreach ( $options as $key => $value ){ ?>
				<option value="<?php echo $key ?>" <?php echo ( $key == $chosen_style ) ? 'selected' : '' ?>><?php echo $value; ?></option>
			<?php } ?>
		</select>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_body_heading_color"><?php _e("Body Heading Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_overlay_body_heading_color" value="<?php echo $overlay_body_heading_color; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_content_style"><?php _e("Select Content Style:", "ca-popup-trigger"); ?></label>
		<select name="ca_overlay_content_style" id="ca-overlay-content-style">
			<?php 
			$options = ca_popup_trigger_msngr_view_settings();
			foreach ( $options as $key => $value ){ ?>
				<option value="<?php echo $key ?>" <?php echo ( $key == $overlay_content_style ) ? 'selected' : '' ?>><?php echo $value; ?></option>
			<?php } ?>
		</select>
	</div>
</div>

<div id="overlay-list-view">
	<div class="ca-row">
		<div class="form-group">
			<label for="ca_overlay_content_one"><?php _e("Content One: (Only 35 Characters)", "ca-popup-trigger"); ?></label>
			<input type="text" placeholder="content one" class="form-table form-control" name="ca_overlay_content_one" value="<?php echo $overlay_content_one; ?>" maxlength="35"/>
		</div>
	</div>
	<div class="ca-row">
		<div class="form-group">
			<label for="ca_overlay_content_two"><?php _e("Content Two: (Only 35 Characters)", "ca-popup-trigger"); ?></label>
			<input type="text" placeholder="content two" class="form-table form-control" name="ca_overlay_content_two" value="<?php echo $overlay_content_two; ?>" maxlength="35"/>
		</div>
	</div>
	<div class="ca-row">
		<div class="form-group">
			<label for="ca_overlay_content_three"><?php _e("Content Three: (Only 35 Characters)", "ca-popup-trigger"); ?></label>
			<input type="text" placeholder="content three" class="form-table form-control" name="ca_overlay_content_three" value="<?php echo $overlay_content_three; ?>" maxlength="35"/>
		</div>
	</div>
</div>

<div id="overlay-message-box">	
	<div class="ca-row">
		<div class="form-group">
			<label for="ca_overlay_full_msg"><?php _e("Message Box", "ca-popup-trigger"); ?></label>
			<textarea name="ca_overlay_full_msg" placeholder="Message box here"><?php echo $overlay_full_msg; ?></textarea>
		</div>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_content_font_color"><?php _e("Content Font Color:","ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_overlay_content_font_color" value="<?php echo $overlay_content_font_color; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_content_position"><?php _e("Content Position:","ca-popup-trigger"); ?></label>
		<select name="ca_overlay_content_position">
			<?php 
			$options = ca_popup_trigger_get_position_settings();
			foreach ( $options as $key => $value ){ ?>
				<option value="<?php echo $key ?>" <?php echo ( $key == $overlay_content_position ) ? 'selected' : '' ?>><?php echo $value; ?></option>
			<?php } ?>
		</select>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_appear_style"><?php _e("Content Position:","ca-popup-trigger"); ?></label>
		<select name="ca_overlay_appear_style">
			<?php 
			$options = ca_popup_trigger_overlay_appear_settings();
			foreach ( $options as $key => $value ){ ?>
				<option value="<?php echo $key ?>" <?php echo ( $key == $overlay_appear_style ) ? 'selected' : '' ?>><?php echo $value; ?></option>
			<?php } ?>
		</select>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_appearing_time"><?php _e("Appear After Time: (ms)","ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_overlay_appearing_time" value="<?php echo $overlay_appearing_time; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_hiding_time"><?php _e("Hide After Time: (ms)","ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_overlay_hiding_time" value="<?php echo $overlay_hiding_time; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_popup_position"><?php _e("PopUp Position:","ca-popup-trigger"); ?></label>
		<select name="ca_overlay_popup_position">
			<?php 
			$options = ca_popup_trigger_get_position_settings();
			foreach ( $options as $key => $value ){ ?>
				<option value="<?php echo $key ?>" <?php echo ( $key == $overlay_popup_position ) ? 'selected' : '' ?>><?php echo $value; ?></option>
			<?php } ?>
		</select>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_cross_btn"><?php _e("Cross Button:", "ca-popup-trigger"); ?></label> 
		<select name="ca_overlay_cross_btn">
			<option value="show" <?php echo ('show' == $overlay_cross_btn) ? 'selected' : '' ?>>Show</option>
			<option value="hide"<?php echo ('hide' == $overlay_cross_btn) ? 'selected' : '' ?>>Hide</option>
		</select>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_visibility_on_page_load"><?php _e("Overlay Visibility:", "ca-popup-trigger"); ?></label> 
		<select name="ca_overlay_visibility_on_page_load">
			<option value="show" <?php echo ('show' == $overlay_visibility_on_load) ? 'selected' : '' ?>>Show</option>
			<option value="hide"<?php echo ('hide' == $overlay_visibility_on_load) ? 'selected' : '' ?>>Hide</option>
		</select>
	</div>
</div>
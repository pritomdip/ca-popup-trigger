<?php
$widget1 				= capt_get_sticky_meta( $post->ID, 'ca_widget', '' );
$widget1_appearing_time = capt_get_sticky_meta( $post->ID, 'ca_widget1_appearing_time', '5000' );
$widget1_hiding_time 	= capt_get_sticky_meta( $post->ID, 'ca_widget1_hiding_time', '20000' );
$widget2 				= capt_get_sticky_meta( $post->ID, 'ca_widget2', '' );
$widget2_appearing_time = capt_get_sticky_meta( $post->ID, 'ca_widget2_appearing_time', '10000' );
$widget2_hiding_time 	= capt_get_sticky_meta( $post->ID, 'ca_widget2_hiding_time', '25000' );
$widget_popup_position 	= capt_get_sticky_meta( $post->ID, 'ca_widget_popup_position','bottom-left');
$widget_visibility 		= capt_get_sticky_meta( $post->ID, 'ca_widget_visibility', 'show' );
?>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_bg_color"><?php _e("HTML Widget ONE:", "ca-popup-trigger"); ?></label>
		<?php 
		$settings = array(
		    'wpautop' 		=> true,
		    'media_buttons' => true,
		    'textarea_rows' => 5,
		    'tinymce' 		=> true,
		    'teeny' 		=> true,
		    'textarea_name' => 'ca_widget',
		    'quicktags' 	=> true,
		    'editor_height' => 300
		   
		);
	    $content = $widget1;
	    $editor_id = 'ca_widget_id';
	    wp_editor(stripslashes($content), $editor_id, $settings);
		?>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_widget1_appearing_time"><?php _e("Widget One Appearing Time: (ms)","ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_widget1_appearing_time" value="<?php echo $widget1_appearing_time; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_widget1_hiding_time"><?php _e("Widget One Hide After Time: (ms)","ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_widget1_hiding_time" value="<?php echo $widget1_hiding_time; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_overlay_bg_color"><?php _e("HTML Widget TWO:", "ca-popup-trigger"); ?></label>
		<?php 
		$settings = array(
		    'wpautop' 		=> true,
		    'media_buttons' => true,
		    'textarea_rows' => 5,
		    'tinymce' 		=> true,
		    'teeny' 		=> true,
		    'textarea_name' => 'ca_widget2',
		    'quicktags' 	=> true,
		    'editor_height' => 300
		   
		);
	    $content = $widget2;
	    $editor_id = 'ca_widget_id2';
	    wp_editor(stripslashes($content), $editor_id, $settings);
		?>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_widget2_appearing_time"><?php _e("Widget Two Appearing Time: (ms)","ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_widget2_appearing_time" value="<?php echo $widget2_appearing_time; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_widget2_hiding_time"><?php _e("Widget Two Hide After Time: (ms)","ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_widget2_hiding_time" value="<?php echo $widget2_hiding_time; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_widget_popup_position"><?php _e("Widget Popup Position:","ca-popup-trigger"); ?></label>
		<select name="ca_widget_popup_position">
			<?php 
			$options = ca_popup_trigger_urgency_popup_positions();
			foreach ( $options as $key => $value ){ ?>
				<option value="<?php echo $key ?>" <?php echo ( $key == $widget_popup_position ) ? 'selected' : '' ?>><?php echo $value; ?></option>
			<?php } ?>
		</select>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_widget_visibility"><?php _e("Widget Visibility:", "ca-popup-trigger"); ?></label> 
		<select name="ca_widget_visibility">
			<option value="show" <?php echo ('show' == $widget_visibility) ? 'selected' : '' ?>>Show</option>
			<option value="hide"<?php echo ('hide' == $widget_visibility) ? 'selected' : '' ?>>Hide</option>
		</select>
	</div>
</div>
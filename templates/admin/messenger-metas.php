<?php 
$msg_bg_color       		= capt_get_sticky_meta( $post->ID, 'ca_msg_bg_color', '#fff' );
$msg_title          		= capt_get_sticky_meta( $post->ID, 'ca_msg_title', 'The title' );
$msg_title_font_clr 		= capt_get_sticky_meta( $post->ID, 'ca_msg_title_font_color', '#9C27B0' );
$msg_title_bg_clr   		= capt_get_sticky_meta( $post->ID, 'ca_msg_title_bg_color', '#CDDC39' );
$msg_btn_link   			= capt_get_sticky_meta( $post->ID, 'ca_msg_btn_link', '#' );
$msg_btn_text   			= capt_get_sticky_meta( $post->ID, 'ca_msg_btn_text', 'Check' );
$msg_btn_text_clr   		= capt_get_sticky_meta( $post->ID, 'ca_msg_btn_text_color', '#60c46b' );
$msg_btn_bg_clr   			= capt_get_sticky_meta( $post->ID, 'ca_msg_btn_bg_color', '#681e32' );
$msg_btn_hover_clr 	 		= capt_get_sticky_meta( $post->ID, 'ca_msg_btn_hover_color', '#000000' );
$msg_img_visibility      	= capt_get_sticky_meta( $post->ID, 'ca_msg_img_visibility', 'no' );
$msg_body_heading      		= capt_get_sticky_meta( $post->ID, 'ca_msg_body_heading', '');
$chosen_style				= capt_get_sticky_meta( $post->ID, 'ca_msg_body_heading_position', 'center');
$msg_body_heading_color		= capt_get_sticky_meta( $post->ID, 'ca_msg_body_heading_color', '#CDDC39');
$msg_content_style			= capt_get_sticky_meta( $post->ID, 'ca_msg_content_style', 'list_view');
$msg_content_one			= capt_get_sticky_meta( $post->ID, 'ca_msg_content_one', '');
$msg_content_two			= capt_get_sticky_meta( $post->ID, 'ca_msg_content_two', '');
$msg_content_three			= capt_get_sticky_meta( $post->ID, 'ca_msg_content_three', '');
$msg_full_msg				= capt_get_sticky_meta( $post->ID, 'ca_msg_full_msg', '');
$msg_content_font_color		= capt_get_sticky_meta( $post->ID, 'ca_msg_content_font_color', '#fff');
$msg_content_position		= capt_get_sticky_meta( $post->ID, 'ca_msg_content_position', 'center');
$msg_visibility_on_load		= capt_get_sticky_meta( $post->ID, 'ca_msg_visibility_on_page_load', 'show');
$msg_visibility				= capt_get_sticky_meta( $post->ID, 'ca_msg_visibility', 'show');
$msg_popup_position			= capt_get_sticky_meta( $post->ID, 'ca_msg_popup_position', 'left');
$msg_cross_btn				= capt_get_sticky_meta( $post->ID, 'ca_msg_cross_btn', 'show');
?>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_msg_bg_color"><?php _e("Background Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_msg_bg_color" value="<?php echo $msg_bg_color; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_msg_title"><?php _e("Title: (Only 30 Characters)", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_msg_title" value="<?php echo $msg_title; ?>" maxlength="30"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_msg_title_font_color"><?php _e("Title Font Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_msg_title_font_color" value="<?php echo $msg_title_font_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_msg_title_bg_color"><?php _e("Title Background Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_msg_title_bg_color" value="<?php echo $msg_title_bg_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_msg_btn_link"><?php _e("Button Link:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_msg_btn_link" value="<?php echo $msg_btn_link; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_msg_btn_text"><?php _e("Button Text:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_msg_btn_text" value="<?php echo $msg_btn_text; ?>" maxlength="11"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_msg_btn_text_color"><?php _e("Button Text Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_msg_btn_text_color" value="<?php echo $msg_btn_text_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_msg_btn_bg_color"><?php _e("Button Background Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_msg_btn_bg_color" value="<?php echo $msg_btn_bg_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_msg_btn_hover_color"><?php _e("Button Mouse Hover Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_msg_btn_hover_color" value="<?php echo $msg_btn_hover_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_msg_img_visibility"><?php _e("Add Image:", "ca-popup-trigger"); ?></label> 
		<select name="ca_msg_img_visibility" id="ca_msg_img_visibility">
			<option value="no"<?php echo ('no' == $msg_img_visibility) ? 'selected' : '' ?>>No</option>
			<option value="yes" <?php echo ('yes' == $msg_img_visibility) ? 'selected' : '' ?>>Yes</option>	
		</select>
	</div>
</div>

<div class="ca-row" id="msg-img">
	<div class="form-group">
		<label for="ca_msg_img_upload"><?php _e("Image Upload:", "ca-popup-trigger"); ?></label>
		<div class="upload">
		<?php
		$ca_img_meta_key 	   = capt_get_sticky_meta( $post->ID, 'ca_msg_image');
		$default_image 		   = plugins_url('ca-popup-trigger/assets/images/room-101.jpg');

		if( !empty( $ca_img_meta_key ) ){
			$image_attributes = wp_get_attachment_image_src( $ca_img_meta_key );
        	$src 			  = empty($image_attributes[0]) ? $default_image : $image_attributes[0];
        	$value 			  = $ca_img_meta_key;
		} else{
			$src 			  = $default_image;
			$value 			  = '';
		}	
		?>
            <img data-src="<?php echo $default_image; ?>" style="display:inline-block;vertical-align: top; margin-left: 20px;" src="<?php echo $src; ?>" width="100px" height="100px" />
            <div style="display:inline-block;vertical-align: top;" >
                <input type="hidden" name="ca_msg_image" id="ca-msg-image" value="<?php echo $value; ?>" />
                <button type="submit" class="upload_image_button button"><?php _e( 'Upload', 'ca-popup-trigger' ); ?></button>
                <button type="submit" class="remove_image_button button">&times;</button>
            </div>

        </div>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_msg_body_heading"><?php _e("Body Heading:", "ca-popup-trigger"); ?></label>
		<input type="text" placeholder="Heading Goes Here" class="form-table form-control" name="ca_msg_body_heading" value="<?php echo $msg_body_heading; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_msg_body_heading_position"><?php _e("Body Heading Position:", "ca-popup-trigger"); ?></label>
		<select name="ca_msg_body_heading_position">
			<?php 
			$options = ca_popup_trigger_get_position_settings();
			foreach ( $options as $key => $value ){ ?>
				<option value="<?php echo $key ?>" <?php echo ( $key == $chosen_style ) ? 'selected' : '' ?>><?php echo $value; ?></option>
			<?php } ?>
		</select>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_msg_body_heading_color"><?php _e("Body Heading Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_msg_body_heading_color" value="<?php echo $msg_body_heading_color; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_msg_content_style"><?php _e("Select Content Style:", "ca-popup-trigger"); ?></label>
		<select name="ca_msg_content_style" id="ca-msg-content-style">
			<?php 
			$options = ca_popup_trigger_msngr_view_settings();
			foreach ( $options as $key => $value ){ ?>
				<option value="<?php echo $key ?>" <?php echo ( $key == $msg_content_style ) ? 'selected' : '' ?>><?php echo $value; ?></option>
			<?php } ?>
		</select>
	</div>
</div>

<div id="list-view">
	<div class="ca-row">
		<div class="form-group">
			<label for="ca_msg_content_one"><?php _e("Content One: (Only 35 Characters)", "ca-popup-trigger"); ?></label>
			<input type="text" placeholder="content one" class="form-table form-control" name="ca_msg_content_one" value="<?php echo $msg_content_one; ?>" maxlength="35"/>
		</div>
	</div>
	<div class="ca-row">
		<div class="form-group">
			<label for="ca_msg_content_two"><?php _e("Content Two: (Only 35 Characters)", "ca-popup-trigger"); ?></label>
			<input type="text" placeholder="content two" class="form-table form-control" name="ca_msg_content_two" value="<?php echo $msg_content_two; ?>" maxlength="35"/>
		</div>
	</div>
	<div class="ca-row">
		<div class="form-group">
			<label for="ca_msg_content_three"><?php _e("Content Three: (Only 35 Characters)", "ca-popup-trigger"); ?></label>
			<input type="text" placeholder="content three" class="form-table form-control" name="ca_msg_content_three" value="<?php echo $msg_content_three; ?>" maxlength="35"/>
		</div>
	</div>
</div>

<div id="message-box">	
	<div class="ca-row">
		<div class="form-group">
			<label for="ca_msg_full_msg"><?php _e("Message Box:", "ca-popup-trigger"); ?></label>
			<textarea name="ca_msg_full_msg" placeholder="Message box message"><?php echo $msg_full_msg; ?></textarea>
		</div>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_msg_content_font_color"><?php _e("Content Font Color:","ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_msg_content_font_color" value="<?php echo $msg_content_font_color; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_msg_content_position"><?php _e("Content Position:","ca-popup-trigger"); ?></label>
		<select name="ca_msg_content_position">
			<?php 
			$options = ca_popup_trigger_get_position_settings();
			foreach ( $options as $key => $value ){ ?>
				<option value="<?php echo $key ?>" <?php echo ( $key == $msg_content_position ) ? 'selected' : '' ?>><?php echo $value; ?></option>
			<?php } ?>
		</select>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_msg_popup_position"><?php _e("PopUp Position:","ca-popup-trigger"); ?></label>
		<select name="ca_msg_popup_position">
			<option value="left" <?php echo ('left' == $msg_popup_position) ? 'selected' : '' ?>>Left</option>
			<option value="right"<?php echo ('right' == $msg_popup_position) ? 'selected' : '' ?>>Right</option>
		</select>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_msg_visibility_on_page_load"><?php _e("Messenger Visibility On Page Load:", "ca-popup-trigger"); ?></label> 
		<select name="ca_msg_visibility_on_page_load">
			<option value="show" <?php echo ('show' == $msg_visibility_on_load) ? 'selected' : '' ?>>Open</option>
			<option value="hide"<?php echo ('hide' == $msg_visibility_on_load) ? 'selected' : '' ?>>Close</option>
		</select>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_msg_visibility"><?php _e("Messenger Show/Hide:", "ca-popup-trigger"); ?></label> 
		<select name="ca_msg_visibility">
			<option value="show" <?php echo ('show' == $msg_visibility) ? 'selected' : '' ?>>Show</option>
			<option value="hide"<?php echo ('hide' == $msg_visibility) ? 'selected' : '' ?>>Hide</option>
		</select>
	</div>
</div>
<?php
$list_bg_color       		= capt_get_sticky_meta( $post->ID, 'ca_list_bg_color', '#fff' );
$list_title          		= capt_get_sticky_meta( $post->ID, 'ca_list_title', 'The title' );
$list_title_font_clr 		= capt_get_sticky_meta( $post->ID, 'ca_list_title_font_color', '#9C27B0' );
$list_title_bg_clr   		= capt_get_sticky_meta( $post->ID, 'ca_list_title_bg_color', '#CDDC39' );
$list_content1   			= capt_get_sticky_meta( $post->ID, 'ca_list_content1','example1');
$list_content2   			= capt_get_sticky_meta( $post->ID, 'ca_list_content2','example2');
$list_content3   			= capt_get_sticky_meta( $post->ID, 'ca_list_content3','example3');
$list_font_color   			= capt_get_sticky_meta( $post->ID, 'ca_list_font_color', '#CDDC39' );
$list_btn_link   			= capt_get_sticky_meta( $post->ID, 'ca_list_btn_link', '#' );
$list_btn_text   			= capt_get_sticky_meta( $post->ID, 'ca_list_btn_text', 'Check' );
$list_btn_text_clr   		= capt_get_sticky_meta( $post->ID, 'ca_list_btn_text_color', '#60c46b' );
$list_btn_bg_clr   			= capt_get_sticky_meta( $post->ID, 'ca_list_btn_bg_color', '#681e32' );
$list_btn_hover_clr 	 	= capt_get_sticky_meta( $post->ID, 'ca_list_btn_hover_color', '#000000' );
$list_visibility_on_load 	= capt_get_sticky_meta( $post->ID, 'ca_list_visibility_on_page_load', 'show' );
$list_visibility  			= capt_get_sticky_meta( $post->ID, 'ca_list_visibility', 'show' );
?>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_list_bg_color"><?php _e("Background Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_list_bg_color" value="<?php echo $list_bg_color; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_list_title"><?php _e("Title: (Only 30 Characters)", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_list_title" value="<?php echo $list_title; ?>" maxlength="30"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_list_title_font_color"><?php _e("Title Font Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_list_title_font_color" value="<?php echo $list_title_font_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_list_title_bg_color"><?php _e("Title Background Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_list_title_bg_color" value="<?php echo $list_title_bg_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_list_content1"><?php _e("Content One: ( Max 25 Characters ) ", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_list_content1" value="<?php echo $list_content1; ?>" maxlength="25"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_list_content2"><?php _e("Content Two: ( Max 25 Characters ) ", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_list_content2" value="<?php echo $list_content2; ?>" maxlength="25"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_list_content3"><?php _e("Content Three: ( Max 25 Characters ) ", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_list_content3" value="<?php echo $list_content3; ?>" maxlength="25"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_list_font_color"><?php _e("Font Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_list_font_color" value="<?php echo $list_font_color; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_list_btn_link"><?php _e("Button Link:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_list_btn_link" value="<?php echo $list_btn_link; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_list_btn_text"><?php _e("Button Text:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_list_btn_text" value="<?php echo $list_btn_text; ?>" maxlength="11"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_list_btn_text_color"><?php _e("Button Text Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_list_btn_text_color" value="<?php echo $list_btn_text_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_list_btn_bg_color"><?php _e("Button Background Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_list_btn_bg_color" value="<?php echo $list_btn_bg_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_list_btn_hover_color"><?php _e("Button Mouse Hover Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_list_btn_hover_color" value="<?php echo $list_btn_hover_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_list_visibility_on_page_load"><?php _e("Sidebar List Visibility On Page Load:", "ca-popup-trigger"); ?></label> 
		<select name="ca_list_visibility_on_page_load">
			<option value="show" <?php echo ('show' == $list_visibility_on_load) ? 'selected' : '' ?>>Open</option>
			<option value="hide"<?php echo ('hide' == $list_visibility_on_load) ? 'selected' : '' ?>>Close</option>
		</select>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_list_visibility"><?php _e("Sidebar List Visibility:", "ca-popup-trigger"); ?></label> 
		<select name="ca_list_visibility">
			<option value="show" <?php echo ('show' == $list_visibility) ? 'selected' : '' ?>>Show</option>
			<option value="hide"<?php echo ('hide' == $list_visibility) ? 'selected' : '' ?>>Hide</option>
		</select>
	</div>
</div>
<?php 
global $post; 
$display_style = capt_get_sticky_meta( $post->ID, 'ca_select_style');
?>

<div class="ca-popup-meta-data">

	<div class="ca-row">
		<div class="form-group">
			<label for="ca-select-popup-style"><?php _e('Select Style:', 'ca-popup-trigger'); ?></label>
			<select id="ca-select-popup-style" name="ca_select_style" required>
			<?php
				$options = capt_display_style();
				foreach ( $options as $key => $value ){ ?>
					<option value="<?php echo $key ?>" <?php echo ( $key == $display_style ) ? 'selected' : '' ?>><?php echo $value; ?></option>
			<?php } ?>
			</select>
		</div>
	</div>

	<div id="chooseone">
	
		<div id="sticky-bar" class="ca-popup-select-class">

			<?php include CAPT_TEMPLATES_DIR . '/admin/sticky-metas.php';  ?>

		</div>

		<div id="sidebar-offer">	

			<?php include CAPT_TEMPLATES_DIR . '/admin/sidebar-metas.php';  ?>

		</div>

		<div id="sidebar-list">

			<?php include CAPT_TEMPLATES_DIR . '/admin/sidebar-list-metas.php';  ?>

		</div>

		<div id="messenger">

			<?php include CAPT_TEMPLATES_DIR . '/admin/messenger-metas.php';  ?>
			
		</div>

		<div id="overlay">

			<?php include CAPT_TEMPLATES_DIR . '/admin/overlay-metas.php';  ?>
			
		</div>

		<div id="urgency-trigger">

			<?php include CAPT_TEMPLATES_DIR . '/admin/urgency-metas.php';  ?>
			
		</div>

		<div id="html-widgets">

			<?php include CAPT_TEMPLATES_DIR . '/admin/html-widget-metas.php';  ?>
			
		</div>

	</div>

</div>

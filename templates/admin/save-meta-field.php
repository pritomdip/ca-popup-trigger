<?php

if( $ca_select_style != '' ){
	update_post_meta( $post_id, 'ca_select_style', $ca_select_style );
}

switch( $ca_select_style ){
	case 'sticky':

		$sticky_title_1 = substr($sticky_title_1, 0, 30);
		$sticky_title_2 = substr($sticky_title_2, 0, 30);
		$sticky_title_3 = substr($sticky_title_3, 0, 30);

		update_post_meta( $post_id, 'ca_sticky_title_1', $sticky_title_1 );
		update_post_meta( $post_id, 'ca_sticky_title_2', $sticky_title_2 );
		update_post_meta( $post_id, 'ca_sticky_title_3', $sticky_title_3 );
		update_post_meta( $post_id, 'ca_sticky_bg_color', $sticky_bg_clr );
		update_post_meta( $post_id, 'ca_sticky_font_color', $sticky_font_clr );
		update_post_meta( $post_id, 'ca_sticky_button_link', $sticky_btn_link );
		update_post_meta( $post_id, 'ca_sticky_button_text', $sticky_btn_text );
		update_post_meta( $post_id, 'ca_sticky_button_text_clr', $sticky_btn_text_clr );
		update_post_meta( $post_id, 'ca_sticky_button_bg_clr', $sticky_btn_bg_clr );
		update_post_meta( $post_id, 'ca_sticky_button_hover_clr', $sticky_btn_hover_clr );
		update_post_meta( $post_id, 'ca_sticky_visibility', $sticky_visibility );
		update_post_meta( $post_id, 'ca_sticky_cross_btn', $sticky_cross_btn );
	break;

	case 'sidebar':

		$sidebar_title    = substr( $sidebar_title, 0, 30 );
		$sidebar_content1 = substr( $sidebar_content1, 0, 11 );
		$sidebar_content2 = substr( $sidebar_content2, 0, 11 );
		$sidebar_content3 = substr( $sidebar_content3, 0, 11 );
		$sidebar_btn_text = substr( $sidebar_btn_text, 0, 11 );
		
		update_post_meta( $post_id, 'ca_sidebar_bg_color', $sidebar_bg_color );
		update_post_meta( $post_id, 'ca_sidebar_title', $sidebar_title );
		update_post_meta( $post_id, 'ca_sidebar_title_font_color', $sidebar_title_font_clr );
		update_post_meta( $post_id, 'ca_sidebar_title_bg_color', $sidebar_title_bg_clr );
		update_post_meta( $post_id, 'ca_sidebar_btn_link', $sidebar_btn_link );
		update_post_meta( $post_id, 'ca_sidebar_btn_text', $sidebar_btn_text );
		update_post_meta( $post_id, 'ca_sidebar_btn_text_color', $sidebar_btn_text_clr );
		update_post_meta( $post_id, 'ca_sidebar_btn_bg_color', $sidebar_btn_bg_clr );
		update_post_meta( $post_id, 'ca_sidebar_btn_hover_color', $sidebar_btn_hover_clr );
		update_post_meta( $post_id, 'ca_sidebar_content1', $sidebar_content1 );
		update_post_meta( $post_id, 'ca_sidebar_content1_color', $sidebar_content1_clr );
		update_post_meta( $post_id, 'ca_sidebar_content2', $sidebar_content2 );
		update_post_meta( $post_id, 'ca_sidebar_content2_color', $sidebar_content2_clr );
		update_post_meta( $post_id, 'ca_sidebar_content3', $sidebar_content3 );
		update_post_meta( $post_id, 'ca_sidebar_content3_color', $sidebar_content3_clr );
		update_post_meta( $post_id, 'ca_sidebar_visibility_on_page_load', $sidebar_visibility_on_load );
		update_post_meta( $post_id, 'ca_sidebar_visibility', $sidebar_visibility );
		
	break;

	case 'list' :

		$list_title    = substr( $list_title, 0, 30 );
		$list_content1 = substr( $list_content1, 0, 25 );
		$list_content2 = substr( $list_content2, 0, 25 );
		$list_content3 = substr( $list_content3, 0, 25 );
		$list_btn_text = substr( $list_btn_text, 0, 11 );

		update_post_meta( $post_id, 'ca_list_bg_color', $list_bg_color );
		update_post_meta( $post_id, 'ca_list_title', $list_title );
		update_post_meta( $post_id, 'ca_list_title_font_color', $list_title_font_clr );
		update_post_meta( $post_id, 'ca_list_title_bg_color', $list_title_bg_clr );
		update_post_meta( $post_id, 'ca_list_content1', $list_content1 );
		update_post_meta( $post_id, 'ca_list_content2', $list_content2 );
		update_post_meta( $post_id, 'ca_list_content3', $list_content3 );
		update_post_meta( $post_id, 'ca_list_font_color', $list_font_color );
		update_post_meta( $post_id, 'ca_list_btn_link', $list_btn_link );
		update_post_meta( $post_id, 'ca_list_btn_text', $list_btn_text );
		update_post_meta( $post_id, 'ca_list_btn_text_color', $list_btn_text_clr );
		update_post_meta( $post_id, 'ca_list_btn_bg_color', $list_btn_bg_clr );
		update_post_meta( $post_id, 'ca_list_btn_hover_color', $list_btn_hover_clr );
		update_post_meta( $post_id, 'ca_list_visibility_on_page_load', $list_visibility_on_load );
		update_post_meta( $post_id, 'ca_list_visibility', $list_visibility );

	break;

	case 'messenger' :

		$msg_content_one 	= substr( $msg_content_one, 0, 35 );
		$msg_content_two 	= substr( $msg_content_two, 0, 35 );
		$msg_content_three	= substr( $msg_content_three, 0, 35 );
		$msg_btn_text 		= substr( $msg_btn_text, 0, 11 );

		if( $msg_img_visibility == 'yes' ){
			update_post_meta( $post_id, 'ca_msg_image', $msg_image );
		}

		if( $msg_content_style === 'list_view' ){
			update_post_meta( $post_id, 'ca_msg_content_one', $msg_content_one );
			update_post_meta( $post_id, 'ca_msg_content_two', $msg_content_two );
			update_post_meta( $post_id, 'ca_msg_content_three', $msg_content_three );
		} else {
			update_post_meta( $post_id, 'ca_msg_full_msg', $msg_full_msg );
		}

		update_post_meta( $post_id, 'ca_msg_bg_color', $msg_bg_color );
		update_post_meta( $post_id, 'ca_msg_title', $msg_title );
		update_post_meta( $post_id, 'ca_msg_title_font_color', $msg_title_font_clr );
		update_post_meta( $post_id, 'ca_msg_title_bg_color', $msg_title_bg_clr );
		update_post_meta( $post_id, 'ca_msg_btn_link', $msg_btn_link );
		update_post_meta( $post_id, 'ca_msg_btn_text', $msg_btn_text );
		update_post_meta( $post_id, 'ca_msg_btn_text_color', $msg_btn_text_clr );
		update_post_meta( $post_id, 'ca_msg_btn_bg_color', $msg_btn_bg_clr );
		update_post_meta( $post_id, 'ca_msg_btn_hover_color', $msg_btn_hover_clr );
		update_post_meta( $post_id, 'ca_msg_img_visibility', $msg_img_visibility );
		update_post_meta( $post_id, 'ca_msg_body_heading', $msg_body_heading );
		update_post_meta( $post_id, 'ca_msg_body_heading_position', $msg_heading_position );
		update_post_meta( $post_id, 'ca_msg_body_heading_color', $msg_heading_color );
		update_post_meta( $post_id, 'ca_msg_content_style', $msg_content_style );
		update_post_meta( $post_id, 'ca_msg_content_font_color', $msg_content_font_color );
		update_post_meta( $post_id, 'ca_msg_content_position', $msg_content_position );
		update_post_meta( $post_id, 'ca_msg_visibility_on_page_load', $msg_visibility_on_load );
		update_post_meta( $post_id, 'ca_msg_popup_position', $msg_popup_position );
		update_post_meta( $post_id, 'ca_msg_visibility', $msg_visibility );

	break;

	case 'overlay' :

		$overlay_content_one 	= substr( $overlay_content_one, 0, 35 );
		$overlay_content_two 	= substr( $overlay_content_two, 0, 35 );
		$overlay_content_three	= substr( $overlay_content_three, 0, 35 );
		$overlay_btn_text 		= substr( $overlay_btn_text, 0, 11 );

		if( $overlay_img_visibility == 'yes' ){
			update_post_meta( $post_id, 'ca_overlay_image', $overlay_image );
		}

		if( $overlay_content_style === 'list_view' ){
			update_post_meta( $post_id, 'ca_overlay_content_one', $overlay_content_one );
			update_post_meta( $post_id, 'ca_overlay_content_two', $overlay_content_two );
			update_post_meta( $post_id, 'ca_overlay_content_three', $overlay_content_three );
		} else {
			update_post_meta( $post_id, 'ca_overlay_full_msg', $overlay_full_msg );
		}

		update_post_meta( $post_id, 'ca_overlay_bg_color', $overlay_bg_color );
		update_post_meta( $post_id, 'ca_overlay_title', $overlay_title );
		update_post_meta( $post_id, 'ca_overlay_title_font_color', $overlay_title_font_clr );
		update_post_meta( $post_id, 'ca_overlay_title_bg_color', $overlay_title_bg_clr );
		update_post_meta( $post_id, 'ca_overlay_title_position', $overlay_title_position );
		update_post_meta( $post_id, 'ca_overlay_btn_link', $overlay_btn_link );
		update_post_meta( $post_id, 'ca_overlay_btn_text', $overlay_btn_text );
		update_post_meta( $post_id, 'ca_overlay_btn_text_color', $overlay_btn_text_clr );
		update_post_meta( $post_id, 'ca_overlay_btn_bg_color', $overlay_btn_bg_clr );
		update_post_meta( $post_id, 'ca_overlay_btn_hover_color', $overlay_btn_hover_clr );
		update_post_meta( $post_id, 'ca_overlay_img_visibility', $overlay_img_visibility );
		update_post_meta( $post_id, 'ca_overlay_body_heading', $overlay_body_heading );
		update_post_meta( $post_id, 'ca_overlay_body_heading_position', $overlay_heading_position );
		update_post_meta( $post_id, 'ca_overlay_body_heading_color', $overlay_heading_color );
		update_post_meta( $post_id, 'ca_overlay_content_style', $overlay_content_style );
		update_post_meta( $post_id, 'ca_overlay_content_font_color', $overlay_content_font_color );
		update_post_meta( $post_id, 'ca_overlay_content_position', $overlay_content_position );
		update_post_meta( $post_id, 'ca_overlay_appear_style', $overlay_appear_style );
		update_post_meta( $post_id, 'ca_overlay_appearing_time', $overlay_appearing_time );
		update_post_meta( $post_id, 'ca_overlay_hiding_time', $overlay_hiding_time );
		update_post_meta( $post_id, 'ca_overlay_popup_position', $overlay_popup_position );
		update_post_meta( $post_id, 'ca_overlay_cross_btn', $overlay_cross_btn );
		update_post_meta( $post_id, 'ca_overlay_visibility_on_page_load', $overlay_visibility_on_load );

	break;

	case 'urgency' :

		$urgency_content1 	= substr( $urgency_content1, 0, 100 );
		$urgency_content2 	= substr( $urgency_content2, 0, 100 );
		$urgency_content3	= substr( $urgency_content3, 0, 100 );

		update_post_meta( $post_id, 'ca_urgency_content1', $urgency_content1 );
		update_post_meta( $post_id, 'ca_urgency_content1_bg_color', $urgency_content1_bg_color );
		update_post_meta( $post_id, 'ca_urgency_content1_font_color', $urgency_content1_font_color );
		update_post_meta( $post_id, 'ca_urgency_content1_appearing_time', $urgency_content1_appearing_time );
		update_post_meta( $post_id, 'ca_urgency_content1_hiding_time',$urgency_content1_hiding_time);
		update_post_meta( $post_id, 'ca_urgency_content1_icon', $urgency_content1_icon );

		update_post_meta( $post_id, 'ca_urgency_content2_visible', $urgency_content2_visible );

		update_post_meta( $post_id, 'ca_urgency_content2', $urgency_content2 );
		update_post_meta( $post_id, 'ca_urgency_content2_bg_color', $urgency_content2_bg_color );
		update_post_meta( $post_id, 'ca_urgency_content2_font_color', $urgency_content2_font_color );
		update_post_meta( $post_id, 'ca_urgency_content2_appearing_time', $urgency_content2_appearing_time );
		update_post_meta( $post_id, 'ca_urgency_content2_hiding_time',$urgency_content2_hiding_time);
		update_post_meta( $post_id, 'ca_urgency_content2_icon', $urgency_content2_icon );

		update_post_meta( $post_id, 'ca_urgency_content3_visible', $urgency_content3_visible );


		update_post_meta( $post_id, 'ca_urgency_content3', $urgency_content3 );
		update_post_meta( $post_id, 'ca_urgency_content3_bg_color', $urgency_content3_bg_color );
		update_post_meta( $post_id, 'ca_urgency_content3_font_color', $urgency_content3_font_color );
		update_post_meta( $post_id, 'ca_urgency_content3_appearing_time', $urgency_content3_appearing_time );
		update_post_meta( $post_id, 'ca_urgency_content3_hiding_time',$urgency_content3_hiding_time);
		update_post_meta( $post_id, 'ca_urgency_content3_icon', $urgency_content3_icon );


		update_post_meta( $post_id, 'ca_urgency_popup_position', $urgency_popup_position );
		update_post_meta( $post_id, 'ca_urgency_cross_btn', $urgency_cross_btn );
		update_post_meta( $post_id, 'ca_urgency_visibility', $urgency_visibility );
	break;

	case 'widget' :

		update_post_meta( $post_id, 'ca_widget', $widget1 );
		update_post_meta( $post_id, 'ca_widget1_appearing_time', $widget1_appearing_time );
		update_post_meta( $post_id, 'ca_widget1_hiding_time', $widget1_hiding_time );
		update_post_meta( $post_id, 'ca_widget2', $widget2 );
		update_post_meta( $post_id, 'ca_widget2_appearing_time', $widget2_appearing_time );
		update_post_meta( $post_id, 'ca_widget2_hiding_time', $widget2_hiding_time );
		update_post_meta( $post_id, 'ca_widget_popup_position', $widget_popup_position );
		update_post_meta( $post_id, 'ca_widget_visibility', $widget_visibility );
		
	break;
		
	default:
}

<?php
$sidebar_bg_color       	= capt_get_sticky_meta( $post->ID, 'ca_sidebar_bg_color', '#fff' );
$sidebar_title          	= capt_get_sticky_meta( $post->ID, 'ca_sidebar_title', 'The title' );
$sidebar_title_font_clr 	= capt_get_sticky_meta( $post->ID, 'ca_sidebar_title_font_color', '#9C27B0' );
$sidebar_title_bg_clr   	= capt_get_sticky_meta( $post->ID, 'ca_sidebar_title_bg_color', '#CDDC39' );
$sidebar_btn_link   		= capt_get_sticky_meta( $post->ID, 'ca_sidebar_btn_link', '#' );
$sidebar_btn_text   		= capt_get_sticky_meta( $post->ID, 'ca_sidebar_btn_text', 'Button' );
$sidebar_btn_text_clr   	= capt_get_sticky_meta( $post->ID, 'ca_sidebar_btn_text_color', '#60c46b' );
$sidebar_btn_bg_clr   		= capt_get_sticky_meta( $post->ID, 'ca_sidebar_btn_bg_color', '#681e32' );
$sidebar_btn_hover_clr 		= capt_get_sticky_meta( $post->ID, 'ca_sidebar_btn_hover_color', '#000000' );
$sidebar_content1          	= capt_get_sticky_meta( $post->ID, 'ca_sidebar_content1', '' );
$sidebar_content1_clr       = capt_get_sticky_meta( $post->ID, 'ca_sidebar_content1_color', '#000' );
$sidebar_content2   		= capt_get_sticky_meta( $post->ID, 'ca_sidebar_content2', '' );
$sidebar_content2_clr    	= capt_get_sticky_meta( $post->ID, 'ca_sidebar_content2_color', '#78f900' );
$sidebar_content3   		= capt_get_sticky_meta( $post->ID, 'ca_sidebar_content3', '' );
$sidebar_content3_clr   	= capt_get_sticky_meta( $post->ID, 'ca_sidebar_content3_color', '#78f900' );
$sidebar_visibility_on_load = capt_get_sticky_meta( $post->ID, 'ca_sidebar_visibility_on_page_load', 'show' );
$sidebar_visibility  		= capt_get_sticky_meta( $post->ID, 'ca_sidebar_visibility', 'show' );
?>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sidebar_bg_color"><?php _e("Background Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_sidebar_bg_color" value="<?php echo $sidebar_bg_color; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sidebar_title"><?php _e("Title: (Only 30 Characters)", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_sidebar_title" value="<?php echo $sidebar_title; ?>" maxlength="30"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sidebar_title_font_color"><?php _e("Title Font Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_sidebar_title_font_color" value="<?php echo $sidebar_title_font_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sidebar_title_bg_color"><?php _e("Title Background Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_sidebar_title_bg_color" value="<?php echo $sidebar_title_bg_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sidebar_btn_link"><?php _e("Button Link:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_sidebar_btn_link" value="<?php echo $sidebar_btn_link; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sidebar_btn_text"><?php _e("Button Text:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_sidebar_btn_text" value="<?php echo $sidebar_btn_text; ?>" maxlength="11"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sidebar_btn_text_color"><?php _e("Button Text Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_sidebar_btn_text_color" value="<?php echo $sidebar_btn_text_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sidebar_btn_bg_color"><?php _e("Button Background Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_sidebar_btn_bg_color" value="<?php echo $sidebar_btn_bg_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sidebar_btn_hover_color"><?php _e("Button Mouse Hover Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_sidebar_btn_hover_color" value="<?php echo $sidebar_btn_hover_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sidebar_content1"><?php _e("Content One: (Only 11 Characters)", "ca-popup-trigger"); ?></label>
		<input type="text" placeholder="Get up to" class="form-table form-control" name="ca_sidebar_content1" value="<?php echo $sidebar_content1; ?>" maxlength="11"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sidebar_content1_color"><?php _e("Content One Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_sidebar_content1_color" value="<?php echo $sidebar_content1_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sidebar_content2"><?php _e("Content Two: (Only 11 Characters)", "ca-popup-trigger"); ?></label>
		<input type="text" placeholder="15%" class="form-table form-control" name="ca_sidebar_content2" value="<?php echo $sidebar_content2; ?>" maxlength="11"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sidebar_offer_amount_color"><?php _e("Content Two Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_sidebar_content2_color" value="<?php echo $sidebar_content2_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sidebar_content3"><?php _e("Content Three: (Only 11 Characters)", "ca-popup-trigger"); ?></label>
		<input type="text" placeholder="Discount" class="form-table form-control" name="ca_sidebar_content3" value="<?php echo $sidebar_content3; ?>" maxlength="11"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sidebar_content3_color"><?php _e("Content Three Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_sidebar_content3_color" value="<?php echo $sidebar_content3_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sidebar_visibility_on_page_load"><?php _e("Sidebar Visibility On Page Load:", "ca-popup-trigger"); ?></label> 
		<select name="ca_sidebar_visibility_on_page_load">
			<option value="show" <?php echo ('show' == $sidebar_visibility_on_load) ? 'selected' : '' ?>>Open</option>
			<option value="hide"<?php echo ('hide' == $sidebar_visibility_on_load) ? 'selected' : '' ?>>Close</option>
		</select>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sidebar_visibility"><?php _e("Sidebar Visibility:", "ca-popup-trigger"); ?></label> 
		<select name="ca_sidebar_visibility">
			<option value="show" <?php echo ('show' == $sidebar_visibility) ? 'selected' : '' ?>>Show</option>
			<option value="hide"<?php echo ('hide' == $sidebar_visibility) ? 'selected' : '' ?>>Hide</option>
		</select>
	</div>
</div>
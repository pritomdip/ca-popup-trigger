<?php
//Sticky option
$ca_select_style = !empty($_POST['ca_select_style']) ? sanitize_text_field($_POST['ca_select_style']) : '';

if( empty( $ca_select_style )){
	return;
}


//Sticky option

if( isset( $ca_select_style ) && $ca_select_style === 'sticky' ) {

	$sticky_title_1   	  = !empty( $_POST['ca_title_1'] ) ? sanitize_text_field( $_POST['ca_title_1'] ) : 'text1';
	$sticky_title_2   	  = !empty( $_POST['ca_title_2'] ) ? sanitize_text_field( $_POST['ca_title_2'] ) : 'text2';
	$sticky_title_3   	  = !empty( $_POST['ca_title_3'] ) ? sanitize_text_field( $_POST['ca_title_3'] ) : 'text3';
	$sticky_bg_clr    	  = !empty( $_POST['ca_sticky_bg_color'] ) ? sanitize_text_field( $_POST['ca_sticky_bg_color'] ) : '';
	$sticky_font_clr  	  = !empty( $_POST['ca_sticky_font_color'] ) ? sanitize_text_field( $_POST['ca_sticky_font_color'] ) : '';
	$sticky_btn_link  	  = !empty( $_POST['ca_sticky_button_link'] ) ? sanitize_text_field( $_POST['ca_sticky_button_link'] ) : '#';
	$sticky_btn_text  	  = !empty( $_POST['ca_sticky_button_text'] ) ? sanitize_text_field( $_POST['ca_sticky_button_text'] ) : 'Button';
	$sticky_btn_text_clr  = !empty( $_POST['ca_sticky_button_text_clr'] ) ? sanitize_text_field( $_POST['ca_sticky_button_text_clr'] ) : '';
	$sticky_btn_bg_clr    = !empty( $_POST['ca_sticky_button_bg_clr'] ) ? sanitize_text_field( $_POST['ca_sticky_button_bg_clr'] ) : '';
	$sticky_btn_hover_clr = !empty( $_POST['ca_sticky_button_hover_clr'] ) ? sanitize_text_field($_POST['ca_sticky_button_hover_clr']) : '';
	$sticky_visibility    = !empty( $_POST['ca_sticky_visibility'] ) ? sanitize_text_field( $_POST['ca_sticky_visibility'] ) : 'show';
	$sticky_cross_btn     = !empty( $_POST['ca_sticky_cross_btn'] ) ? sanitize_text_field( $_POST['ca_sticky_cross_btn'] ) : 'show';
}

// Sidebar Option

if( isset( $ca_select_style ) && $ca_select_style === 'sidebar' ) {

	$sidebar_bg_color  		 	= !empty( $_POST['ca_sidebar_bg_color'] ) ? sanitize_text_field( $_POST['ca_sidebar_bg_color'] ) : '';
	$sidebar_title  			= !empty( $_POST['ca_sidebar_title'] ) ? sanitize_text_field( $_POST['ca_sidebar_title'] ) : 'The title';
	$sidebar_title_font_clr  	= !empty( $_POST['ca_sidebar_title_font_color'] ) ? sanitize_text_field( $_POST['ca_sidebar_title_font_color'] ) : '';
	$sidebar_title_bg_clr  	 	= !empty( $_POST['ca_sidebar_title_bg_color'] ) ? sanitize_text_field( $_POST['ca_sidebar_title_bg_color'] ) : '';
	$sidebar_btn_link  			= !empty( $_POST['ca_sidebar_btn_link'] ) ? sanitize_text_field( $_POST['ca_sidebar_btn_link'] ) : '#';
	$sidebar_btn_text  			= !empty( $_POST['ca_sidebar_btn_text'] ) ? sanitize_text_field( $_POST['ca_sidebar_btn_text'] ) : 'Button';
	$sidebar_btn_text_clr  		= !empty( $_POST['ca_sidebar_btn_text_color'] ) ? sanitize_text_field( $_POST['ca_sidebar_btn_text_color'] ) : '';
	$sidebar_btn_bg_clr  		= !empty( $_POST['ca_sidebar_btn_bg_color'] ) ? sanitize_text_field( $_POST['ca_sidebar_btn_bg_color'] ) : '';
	$sidebar_btn_hover_clr  	= !empty( $_POST['ca_sidebar_btn_hover_color'] ) ? sanitize_text_field( $_POST['ca_sidebar_btn_hover_color'] ) : '';
	$sidebar_content1  			= !empty( $_POST['ca_sidebar_content1'] ) ? sanitize_text_field( $_POST['ca_sidebar_content1'] ) : '';
	$sidebar_content1_clr  		= !empty( $_POST['ca_sidebar_content1_color'] ) ? sanitize_text_field( $_POST['ca_sidebar_content1_color'] ) : '';
	$sidebar_content2  			= !empty( $_POST['ca_sidebar_content2'] ) ? sanitize_text_field( $_POST['ca_sidebar_content2'] ) : '';
	$sidebar_content2_clr   	= !empty( $_POST['ca_sidebar_content2_color'] ) ? sanitize_text_field( $_POST['ca_sidebar_content2_color'] ) : '';
	$sidebar_content3  			= !empty( $_POST['ca_sidebar_content3'] ) ? sanitize_text_field( $_POST['ca_sidebar_content3'] ) : '';
	$sidebar_content3_clr  		= !empty( $_POST['ca_sidebar_content3_color'] ) ? sanitize_text_field( $_POST['ca_sidebar_content3_color'] ) : '';
	$sidebar_visibility_on_load = !empty( $_POST['ca_sidebar_visibility_on_page_load'] ) ? sanitize_text_field( $_POST['ca_sidebar_visibility_on_page_load'] ) : 'show';
	$sidebar_visibility   		= !empty( $_POST['ca_sidebar_visibility'] ) ? sanitize_text_field( $_POST['ca_sidebar_visibility'] ) : 'show';

}

// Sidebar List Option

if( isset( $ca_select_style ) && $ca_select_style === 'list' ) {

	$list_bg_color  		 	= !empty( $_POST['ca_list_bg_color'] ) ? sanitize_text_field( $_POST['ca_list_bg_color'] ) : '';
	$list_title  		 		= !empty( $_POST['ca_list_title'] ) ? sanitize_text_field( $_POST['ca_list_title'] ) : 'The title';
	$list_title_font_clr  		= !empty( $_POST['ca_list_title_font_color'] ) ? sanitize_text_field( $_POST['ca_list_title_font_color'] ) : '';
	$list_title_bg_clr  		= !empty( $_POST['ca_list_title_bg_color'] ) ? sanitize_text_field( $_POST['ca_list_title_bg_color'] ) : '';
	$list_content1  		 	= !empty( $_POST['ca_list_content1'] ) ? sanitize_text_field( $_POST['ca_list_content1'] ) : 'example1';
	$list_content2  		 	= !empty( $_POST['ca_list_content2'] ) ? sanitize_text_field( $_POST['ca_list_content2'] ) : 'example2';
	$list_content3  		 	= !empty( $_POST['ca_list_content3'] ) ? sanitize_text_field( $_POST['ca_list_content3'] ) : 'example3';
	$list_font_color  		 	= !empty( $_POST['ca_list_font_color'] ) ? sanitize_text_field( $_POST['ca_list_font_color'] ) : '';
	$list_btn_link  		 	= !empty( $_POST['ca_list_btn_link'] ) ? sanitize_text_field( $_POST['ca_list_btn_link'] ) : '#';
	$list_btn_text  		 	= !empty( $_POST['ca_list_btn_text'] ) ? sanitize_text_field( $_POST['ca_list_btn_text'] ) : 'Button';
	$list_btn_text_clr  		= !empty( $_POST['ca_list_btn_text_color'] ) ? sanitize_text_field( $_POST['ca_list_btn_text_color'] ) : '';
	$list_btn_bg_clr  		 	= !empty( $_POST['ca_list_btn_bg_color'] ) ? sanitize_text_field( $_POST['ca_list_btn_bg_color'] ) : '';
	$list_btn_hover_clr  		= !empty( $_POST['ca_list_btn_hover_color'] ) ? sanitize_text_field( $_POST['ca_list_btn_hover_color'] ) : '';
	$list_visibility_on_load 	= !empty( $_POST['ca_list_visibility_on_page_load'] ) ? sanitize_text_field( $_POST['ca_list_visibility_on_page_load'] ) : 'show';
	$list_visibility   			= !empty( $_POST['ca_list_visibility'] ) ? sanitize_text_field( $_POST['ca_list_visibility'] ) : 'show';

}

// Messenger Option

if( isset( $ca_select_style ) && $ca_select_style === 'messenger' ) {

	$msg_bg_color  		 	= !empty( $_POST['ca_msg_bg_color'] ) ? sanitize_text_field( $_POST['ca_msg_bg_color'] ) : '';
	$msg_title  		 	= !empty( $_POST['ca_msg_title'] ) ? sanitize_text_field( $_POST['ca_msg_title'] ) : 'The title';
	$msg_title_font_clr  	= !empty( $_POST['ca_msg_title_font_color'] ) ? sanitize_text_field( $_POST['ca_msg_title_font_color'] ) : '';
	$msg_title_bg_clr  		= !empty( $_POST['ca_msg_title_bg_color'] ) ? sanitize_text_field( $_POST['ca_msg_title_bg_color'] ) : '';
	$msg_btn_link  		 	= !empty( $_POST['ca_msg_btn_link'] ) ? sanitize_text_field( $_POST['ca_msg_btn_link'] ) : '#';
	$msg_btn_text  		 	= !empty( $_POST['ca_msg_btn_text'] ) ? sanitize_text_field( $_POST['ca_msg_btn_text'] ) : 'Button';
	$msg_btn_text_clr  		= !empty( $_POST['ca_msg_btn_text_color'] ) ? sanitize_text_field( $_POST['ca_msg_btn_text_color'] ) : '';
	$msg_btn_bg_clr  		= !empty( $_POST['ca_msg_btn_bg_color'] ) ? sanitize_text_field( $_POST['ca_msg_btn_bg_color'] ) : '';
	$msg_btn_hover_clr  	= !empty( $_POST['ca_msg_btn_hover_color'] ) ? sanitize_text_field( $_POST['ca_msg_btn_hover_color'] ) : '';
	$msg_img_visibility  	= !empty( $_POST['ca_msg_img_visibility'] ) ? sanitize_text_field( $_POST['ca_msg_img_visibility'] ) : 'no';
	$msg_image  			= !empty( $_POST['ca_msg_image'] ) ? intval( $_POST['ca_msg_image'] ) : 'no';
	$msg_body_heading  		= !empty( $_POST['ca_msg_body_heading'] ) ? sanitize_text_field( $_POST['ca_msg_body_heading'] ) : '';
	$msg_heading_position  	= !empty( $_POST['ca_msg_body_heading_position'] ) ? sanitize_text_field( $_POST['ca_msg_body_heading_position'] ) : 'center';
	$msg_heading_color  	= !empty( $_POST['ca_msg_body_heading_color'] ) ? sanitize_text_field( $_POST['ca_msg_body_heading_color'] ) : '';
	$msg_content_style  	= !empty( $_POST['ca_msg_content_style'] ) ? sanitize_text_field( $_POST['ca_msg_content_style'] ) : 'list_view';
	$msg_content_one 	 	= !empty( $_POST['ca_msg_content_one'] ) ? sanitize_text_field( $_POST['ca_msg_content_one'] ) : '';
	$msg_content_two 	 	= !empty( $_POST['ca_msg_content_two'] ) ? sanitize_text_field( $_POST['ca_msg_content_two'] ) : '';
	$msg_content_three 	 	= !empty( $_POST['ca_msg_content_three'] ) ? sanitize_text_field( $_POST['ca_msg_content_three'] ) : '';
	$msg_full_msg 	 		= !empty( $_POST['ca_msg_full_msg'] ) ? esc_html( $_POST['ca_msg_full_msg'] ) : '';
	$msg_content_font_color = !empty( $_POST['ca_msg_content_font_color'] ) ? sanitize_text_field( $_POST['ca_msg_content_font_color'] ) : '';
	$msg_content_position   = !empty( $_POST['ca_msg_content_position'] ) ? sanitize_text_field( $_POST['ca_msg_content_position'] ) : '';
	$msg_visibility_on_load = !empty( $_POST['ca_msg_visibility_on_page_load'] ) ? sanitize_text_field( $_POST['ca_msg_visibility_on_page_load'] ) : '';
	$msg_popup_position 	= !empty( $_POST['ca_msg_popup_position'] ) ? sanitize_text_field( $_POST['ca_msg_popup_position'] ) : '';
	$msg_visibility 		= !empty( $_POST['ca_msg_visibility'] ) ? sanitize_text_field( $_POST['ca_msg_visibility'] ) : '';

}

if( isset( $ca_select_style ) && $ca_select_style === 'overlay' ) {

	$overlay_bg_color  		 	= !empty( $_POST['ca_overlay_bg_color'] ) ? sanitize_text_field( $_POST['ca_overlay_bg_color'] ) : '';
	$overlay_title  		 	= !empty( $_POST['ca_overlay_title'] ) ? sanitize_text_field( $_POST['ca_overlay_title'] ) : 'The title';
	$overlay_title_font_clr  	= !empty( $_POST['ca_overlay_title_font_color'] ) ? sanitize_text_field( $_POST['ca_overlay_title_font_color'] ) : '';
	$overlay_title_bg_clr  		= !empty( $_POST['ca_overlay_title_bg_color'] ) ? sanitize_text_field( $_POST['ca_overlay_title_bg_color'] ) : '';
	$overlay_title_position  	= !empty( $_POST['ca_overlay_title_position'] ) ? sanitize_text_field( $_POST['ca_overlay_title_position'] ) : '';
	$overlay_btn_link  		 	= !empty( $_POST['ca_overlay_btn_link'] ) ? sanitize_text_field( $_POST['ca_overlay_btn_link'] ) : '#';
	$overlay_btn_text  		 	= !empty( $_POST['ca_overlay_btn_text'] ) ? sanitize_text_field( $_POST['ca_overlay_btn_text'] ) : 'Button';
	$overlay_btn_text_clr  		= !empty( $_POST['ca_overlay_btn_text_color'] ) ? sanitize_text_field( $_POST['ca_overlay_btn_text_color'] ) : '';
	$overlay_btn_bg_clr  		= !empty( $_POST['ca_overlay_btn_bg_color'] ) ? sanitize_text_field( $_POST['ca_overlay_btn_bg_color'] ) : '';
	$overlay_btn_hover_clr  	= !empty( $_POST['ca_overlay_btn_hover_color'] ) ? sanitize_text_field( $_POST['ca_overlay_btn_hover_color'] ) : '';
	$overlay_img_visibility  	= !empty( $_POST['ca_overlay_img_visibility'] ) ? sanitize_text_field( $_POST['ca_overlay_img_visibility'] ) : 'no';
	$overlay_image  			= !empty( $_POST['ca_overlay_image'] ) ? intval( $_POST['ca_overlay_image'] ) : 'no';
	$overlay_body_heading  		= !empty( $_POST['ca_overlay_body_heading'] ) ? sanitize_text_field( $_POST['ca_overlay_body_heading'] ) : '';
	$overlay_heading_position  	= !empty( $_POST['ca_overlay_body_heading_position'] ) ? sanitize_text_field( $_POST['ca_overlay_body_heading_position'] ) : 'center';
	$overlay_heading_color  	= !empty( $_POST['ca_overlay_body_heading_color'] ) ? sanitize_text_field( $_POST['ca_overlay_body_heading_color'] ) : '';
	$overlay_content_style  	= !empty( $_POST['ca_overlay_content_style'] ) ? sanitize_text_field( $_POST['ca_overlay_content_style'] ) : 'list_view';
	$overlay_content_one 	 	= !empty( $_POST['ca_overlay_content_one'] ) ? sanitize_text_field( $_POST['ca_overlay_content_one'] ) : '';
	$overlay_content_two 	 	= !empty( $_POST['ca_overlay_content_two'] ) ? sanitize_text_field( $_POST['ca_overlay_content_two'] ) : '';
	$overlay_content_three 	 	= !empty( $_POST['ca_overlay_content_three'] ) ? sanitize_text_field( $_POST['ca_overlay_content_three'] ) : '';
	$overlay_full_msg 	 		= !empty( $_POST['ca_overlay_full_msg'] ) ? esc_html( $_POST['ca_overlay_full_msg'] ) : '';
	$overlay_content_font_color = !empty( $_POST['ca_overlay_content_font_color'] ) ? sanitize_text_field( $_POST['ca_overlay_content_font_color'] ) : '';
	$overlay_content_position   = !empty( $_POST['ca_overlay_content_position'] ) ? sanitize_text_field( $_POST['ca_overlay_content_position'] ) : '';
	$overlay_appear_style   	= !empty( $_POST['ca_overlay_appear_style'] ) ? sanitize_text_field( $_POST['ca_overlay_appear_style'] ) : 'fadeInLeft';
	$overlay_appearing_time   	= !empty( $_POST['ca_overlay_appearing_time'] ) ? sanitize_text_field( $_POST['ca_overlay_appearing_time'] ) : '';
	$overlay_hiding_time   		= !empty( $_POST['ca_overlay_hiding_time'] ) ? sanitize_text_field( $_POST['ca_overlay_hiding_time'] ) : '';
	$overlay_popup_position 	= !empty( $_POST['ca_overlay_popup_position'] ) ? sanitize_text_field( $_POST['ca_overlay_popup_position'] ) : '';
	$overlay_cross_btn 			= !empty( $_POST['ca_overlay_cross_btn'] ) ? sanitize_text_field( $_POST['ca_overlay_cross_btn'] ) : '';
	$overlay_visibility_on_load = !empty( $_POST['ca_overlay_visibility_on_page_load'] ) ? sanitize_text_field( $_POST['ca_overlay_visibility_on_page_load'] ) : '';

}

if( isset( $ca_select_style ) && $ca_select_style === 'urgency' ) {

	$urgency_content1  		   	 	 = !empty( $_POST['ca_urgency_content1'] ) ? sanitize_text_field( $_POST['ca_urgency_content1'] ) : 'Content one';
	$urgency_content1_bg_color 	 	 = !empty( $_POST['ca_urgency_content1_bg_color'] ) ? sanitize_text_field( $_POST['ca_urgency_content1_bg_color'] ) : '';
	$urgency_content1_font_color 	 = !empty( $_POST['ca_urgency_content1_font_color'] ) ? sanitize_text_field( $_POST['ca_urgency_content1_font_color'] ) : '';
	$urgency_content1_appearing_time = !empty( $_POST['ca_urgency_content1_appearing_time'] ) ? sanitize_text_field( $_POST['ca_urgency_content1_appearing_time'] ) : '';
	$urgency_content1_hiding_time 	 = !empty( $_POST['ca_urgency_content1_hiding_time'] ) ? sanitize_text_field( $_POST['ca_urgency_content1_hiding_time'] ) : '';
	$urgency_content1_icon 	 		 = !empty( $_POST['ca_urgency_content1_icon'] ) ? sanitize_text_field( $_POST['ca_urgency_content1_icon'] ) : '';
	$urgency_content2_visible 	 	 = !empty( $_POST['ca_urgency_content2_visible'] ) ? sanitize_text_field( $_POST['ca_urgency_content2_visible'] ) : '';
	$urgency_content2  		   	 	 = !empty( $_POST['ca_urgency_content2'] ) ? sanitize_text_field( $_POST['ca_urgency_content2'] ) : '';
	$urgency_content2_bg_color 	 	 = !empty( $_POST['ca_urgency_content2_bg_color'] ) ? sanitize_text_field( $_POST['ca_urgency_content2_bg_color'] ) : '';
	$urgency_content2_font_color 	 = !empty( $_POST['ca_urgency_content2_font_color'] ) ? sanitize_text_field( $_POST['ca_urgency_content2_font_color'] ) : '';
	$urgency_content2_appearing_time = !empty( $_POST['ca_urgency_content2_appearing_time'] ) ? sanitize_text_field( $_POST['ca_urgency_content2_appearing_time'] ) : '';
	$urgency_content2_hiding_time 	 = !empty( $_POST['ca_urgency_content2_hiding_time'] ) ? sanitize_text_field( $_POST['ca_urgency_content2_hiding_time'] ) : '';
	$urgency_content2_icon 	 		 = !empty( $_POST['ca_urgency_content2_icon'] ) ? sanitize_text_field( $_POST['ca_urgency_content2_icon'] ) : '';
	$urgency_content3_visible 	 	 = !empty( $_POST['ca_urgency_content3_visible'] ) ? sanitize_text_field( $_POST['ca_urgency_content3_visible'] ) : '';
	$urgency_content3  		   	 	 = !empty( $_POST['ca_urgency_content3'] ) ? sanitize_text_field( $_POST['ca_urgency_content3'] ) : '';
	$urgency_content3_bg_color 	 	 = !empty( $_POST['ca_urgency_content3_bg_color'] ) ? sanitize_text_field( $_POST['ca_urgency_content3_bg_color'] ) : '';
	$urgency_content3_font_color 	 = !empty( $_POST['ca_urgency_content3_font_color'] ) ? sanitize_text_field( $_POST['ca_urgency_content3_font_color'] ) : '';
	$urgency_content3_appearing_time = !empty( $_POST['ca_urgency_content3_appearing_time'] ) ? sanitize_text_field( $_POST['ca_urgency_content3_appearing_time'] ) : '';
	$urgency_content3_hiding_time 	 = !empty( $_POST['ca_urgency_content3_hiding_time'] ) ? sanitize_text_field( $_POST['ca_urgency_content3_hiding_time'] ) : '';
	$urgency_content3_icon 	 		 = !empty( $_POST['ca_urgency_content3_icon'] ) ? sanitize_text_field( $_POST['ca_urgency_content3_icon'] ) : '';
	$urgency_popup_position 	 	 = !empty( $_POST['ca_urgency_popup_position'] ) ? sanitize_text_field( $_POST['ca_urgency_popup_position'] ) : '';
	$urgency_cross_btn 	 		 	 = !empty( $_POST['ca_urgency_cross_btn'] ) ? sanitize_text_field( $_POST['ca_urgency_cross_btn'] ) : '';
	$urgency_visibility 	 		 = !empty( $_POST['ca_urgency_visibility'] ) ? sanitize_text_field( $_POST['ca_urgency_visibility'] ) : '';

}

if( isset( $ca_select_style ) && $ca_select_style === 'widget' ) {

	$widget1 = !empty( $_POST['ca_widget'] ) ?  $_POST['ca_widget']  : '';
	$widget1_appearing_time = !empty( $_POST['ca_widget1_appearing_time'] ) ? sanitize_text_field( $_POST['ca_widget1_appearing_time'] ) : '';
	$widget1_hiding_time = !empty( $_POST['ca_widget1_hiding_time'] ) ? sanitize_text_field( $_POST['ca_widget1_hiding_time'] ) : '';


	$widget2 = !empty( $_POST['ca_widget2'] ) ?  $_POST['ca_widget2']  : '';
	$widget2_appearing_time = !empty( $_POST['ca_widget2_appearing_time'] ) ? sanitize_text_field( $_POST['ca_widget2_appearing_time'] ) : '';
	$widget2_hiding_time = !empty( $_POST['ca_widget2_hiding_time'] ) ? sanitize_text_field( $_POST['ca_widget2_hiding_time'] ) : '';


	$widget_popup_position = !empty( $_POST['ca_widget_popup_position'] ) ? sanitize_text_field( $_POST['ca_widget_popup_position'] ) : '';
	$widget_visibility = !empty( $_POST['ca_widget_visibility'] ) ? sanitize_text_field( $_POST['ca_widget_visibility'] ) : '';

}
<?php
$title1 	   				= capt_get_sticky_meta( $post->ID, 'ca_sticky_title_1', 'Text1' );
$title2 	   				= capt_get_sticky_meta( $post->ID, 'ca_sticky_title_2', 'Text2' );
$title3 	  				= capt_get_sticky_meta( $post->ID, 'ca_sticky_title_3', 'Text3' );
$sticky_bg_clr 				= capt_get_sticky_meta( $post->ID, 'ca_sticky_bg_color', '#31B0D5' );
$sticky_font_clr 			= capt_get_sticky_meta( $post->ID, 'ca_sticky_font_color', '#fff' );
$sticky_btn_link  			= capt_get_sticky_meta( $post->ID, 'ca_sticky_button_link', '#' );
$sticky_btn_text  			= capt_get_sticky_meta( $post->ID, 'ca_sticky_button_text', "Button");
$sticky_btn_text_clr 		= capt_get_sticky_meta( $post->ID, 'ca_sticky_button_text_clr', '#fff' );
$sticky_btn_bg_clr  		= capt_get_sticky_meta( $post->ID, 'ca_sticky_button_bg_clr', '#ddd' );
$sticky_btn_hover_clr   	= capt_get_sticky_meta( $post->ID, 'ca_sticky_button_hover_clr', '#dd3333' );
$sticky_visibility  		= capt_get_sticky_meta( $post->ID, 'ca_sticky_visibility', 'show' );
$sticky_cross_btn  			= capt_get_sticky_meta( $post->ID, 'ca_sticky_cross_btn', 'show' );
?>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sticky_bg_color"><?php _e("Background Color:", "ca-popup-trigger"); ?></label> 
		<input type="text" class="form-table form-control ca-color-field" name="ca_sticky_bg_color" value="<?php echo $sticky_bg_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sticky_button_link"><?php _e("Sticky Button Link:", "ca-popup-trigger"); ?></label> 
		<input type="text" class="form-table form-control" name="ca_sticky_button_link" value="<?php echo $sticky_btn_link; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sticky_button_text"><?php _e("Sticky Button Text:", "ca-popup-trigger"); ?></label> 
		<input type="text" class="form-table form-control" name="ca_sticky_button_text" value="<?php echo $sticky_btn_text; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sticky_button_text_clr"><?php _e("Sticky Button Text Color:", "ca-popup-trigger"); ?></label> 
		<input type="text" class="form-table form-control ca-color-field" name="ca_sticky_button_text_clr" value="<?php echo $sticky_btn_text_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sticky_button_bg_clr"><?php _e("Sticky Button Background Color:", "ca-popup-trigger"); ?></label> 
		<input type="text" class="form-table form-control ca-color-field" name="ca_sticky_button_bg_clr" value="<?php echo $sticky_btn_bg_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sticky_button_hover_clr"><?php _e("Sticky Button Mouse Hover Color:", "ca-popup-trigger"); ?></label> 
		<input type="text" class="form-table form-control ca-color-field" name="ca_sticky_button_hover_clr" value="<?php echo $sticky_btn_hover_clr; ?>"/>
	</div>
</div>

<div class="ca-row">
 	<div class="form-group">
		<label for="ca_title_1"><?php _e("Title 1: (Only 30 Characters)", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_title_1" value="<?php echo $title1; ?>" maxlength="30"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_title_2"><?php _e("Title 2: (Only 30 Characters)", "ca-popup-trigger"); ?></label> 
		<input type="text" class="form-table form-control" name="ca_title_2" value="<?php echo $title2; ?>" maxlength="30"/>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_title_3"><?php _e("Title 3: (Only 30 Characters)", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control" name="ca_title_3" value="<?php echo $title3; ?>" maxlength="30"/>
	</div>
</div>	

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sticky_font_color"><?php _e("Sticky Font Color:", "ca-popup-trigger"); ?></label>
		<input type="text" class="form-table form-control ca-color-field" name="ca_sticky_font_color" value="<?php echo $sticky_font_clr; ?>"/>
	</div>
</div>


<div class="ca-row">
	<div class="form-group">
		<label for="ca_sticky_visibility"><?php _e("Sticky Visibility:", "ca-popup-trigger"); ?></label> 
		<select name="ca_sticky_visibility">
			<option value="show" <?php echo ('show' == $sticky_visibility) ? 'selected' : '' ?>>Show</option>
			<option value="hide"<?php echo ('hide' == $sticky_visibility) ? 'selected' : '' ?>>Hide</option>
		</select>
	</div>
</div>

<div class="ca-row">
	<div class="form-group">
		<label for="ca_sticky_cross_btn"><?php _e("Cross Button:", "ca-popup-trigger"); ?></label> 
		<select name="ca_sticky_cross_btn">
			<option value="show" <?php echo ('show' == $sticky_cross_btn) ? 'selected' : '' ?>>Show</option>
			<option value="hide"<?php echo ('hide' == $sticky_cross_btn) ? 'selected' : '' ?>>Hide</option>
		</select>
	</div>
</div>
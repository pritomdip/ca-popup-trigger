<div id="ca-sidebar-id ca-sidebar-list-id" class="ca-sidebar-wrapper ca-sidebar-list-wrapper">
  	<div id='ca-sidebar-left-wrapper' class="ca-sidebar-left ca-sidebar-list <?php echo $class; ?> ">	
	    
	    <div class='ca-sidebar-left-content' style="background:<?php echo $settings['ca_list_bg_color']; ?>;">
	    	<div class="ca-content-wrapper">
	    		<div class="ca-height">
		    		<div class="content-1" style="color:<?php echo $settings['ca_list_font_color']; ?>";><div class="checkmark">♦</div><?php echo wp_kses_post( $settings['ca_list_content1'] ); ?></div>
		    		<div class="content-1" style="color:<?php echo $settings['ca_list_font_color']; ?>";><div class="checkmark">♦</div><?php echo wp_kses_post( $settings['ca_list_content2'] ); ?></div>
		    		<div class="content-1" style="color:<?php echo $settings['ca_list_font_color']; ?>";><div class="checkmark">♦</div><?php echo wp_kses_post( $settings['ca_list_content3'] ); ?></div>
		    	</div>
	    		<a href="<?php echo esc_url($settings['ca_list_btn_link']); ?>">
	    			<button class="ca-sidebar-btn" style="background:<?php echo $settings['ca_list_btn_bg_color']; ?>; color:<?php echo $settings['ca_list_btn_text_color']; ?>;" onMouseOver='this.style.color="<?php echo $settings['ca_list_btn_hover_color']; ?>"' onMouseOut='this.style.color="<?php echo $settings['ca_list_btn_text_color']; ?>"'><?php echo wp_kses_post($settings['ca_list_btn_text']); ?></button>
	    		</a>
	    	</div>
	    </div>

	    <div class='ca-sidebar-left-trigger' style="background:<?php echo $settings['ca_list_title_bg_color']; ?>;">
  			<div class="ca-sidebar-title">
  				<div class="ca-title-wrapper">
					<div class="ca-triangle <?php echo $icon; ?>"></div>
				</div>
  				<div class="ca-title-wrapper ca-title-content-wrapper" style="color: <?php echo $settings['ca_list_title_font_color']; ?>;">
  					<?php echo wp_kses_post( $settings['ca_list_title'] ); ?>		
  				</div>	
  			</div>					     	
	    </div>

  	</div>
</div>
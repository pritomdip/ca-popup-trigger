<div class=" ca-popup-messenger-wrapper <?php echo $class; ?>">
    <div class="ca-popup-messenger-position">

        <div class="ca-messenger-popup-title" style="background:<?php echo $settings['ca_msg_title_bg_color']; ?>">
            
            <div class="ca-up-down-arrow">   
                <div class="triangle down"></div>    
                <div class="triangle up"></div>
            </div>

            <a class="" style=" color:<?php echo $settings['ca_msg_title_font_color']; ?>;"><?php echo wp_kses_post( $settings['ca_msg_title'] ); ?></a>
        </div>

        <div class="<?php echo $visibility; ?> ca-popup-messenger-content-wrapper" style="background:<?php echo $settings['ca_msg_bg_color']; ?>;">

            <div class="js-tab__tab">
                <div class="ca-msngr-image-wrapper">

                <?php
                    $src = ca_popup_trigger_get_image( $post_id, $settings['ca_msg_img_visibility'] );
                    if( !empty( $src ) ){
                ?>
                    <img class="ca-msngr-image-wrapper" src="<?php echo $src; ?>" alt="image">
                <?php } ?>
                </div>

                <div class="ca-msngr-content-wrapper" style="color:<?php echo $settings['ca_msg_content_font_color']; ?>;">

                <?php if( $settings['ca_msg_content_style'] === 'message_box' ){ ?>

                    <div class="ca-msg-box">
                        <div><?php echo esc_html( $settings['ca_msg_full_msg'] ); ?></div>
                    </div>

                <?php } else { ?>

                    <div class="ca-msngr-list-view">

                        <?php if(!empty($settings['ca_msg_body_heading'])){ ?>

                        <h5 class="ca-msngr-title <?php echo $settings['ca_msg_body_heading_position']; ?>" style="color: <?php echo $settings['ca_msg_body_heading_color']; ?>"><?php echo wp_kses_post( $settings['ca_msg_body_heading'] ); ?></h5>

                        <?php } ?>

                        <div class="ca-msngr-list-content-wrapper <?php echo $settings['ca_msg_content_position']; ?>">

                            <?php if(!empty($settings['ca_msg_content_one'])){ ?>

                            <div class="ca-content">
                                <span class="ca-checkmark"><?php _e('&#10003', 'ca-popup-trigger'); ?></span>
                                <span><?php echo wp_kses_post( $settings['ca_msg_content_one'] ); ?></span>
                            </div>

                            <?php }
                            if(!empty($settings['ca_msg_content_two'])){ ?>

                            <div class="ca-content">
                                <span class="ca-checkmark"><?php _e('&#10003', 'ca-popup-trigger'); ?></span>
                                <span><?php echo wp_kses_post( $settings['ca_msg_content_two'] ); ?></span>
                            </div>

                            <?php } 
                            if(!empty($settings['ca_msg_content_three'])){ ?>

                            <div class="ca-content">
                                <span class="ca-checkmark"><?php _e('&#10003', 'ca-popup-trigger'); ?></span>
                                <span><?php echo wp_kses_post( $settings['ca_msg_content_three'] ); ?></span>
                            </div>

                            <?php } ?>

                        </div>
                    </div>
                <?php } ?>
                </div>
                
                <a href="<?php echo esc_url($settings['ca_msg_btn_link']); ?>" class="ca-msngr-btn-link">
                    <button class="ca-msngr-btn" style="background:<?php echo $settings['ca_msg_btn_bg_color']; ?>; color:<?php echo $settings['ca_msg_btn_text_color']; ?>;" onMouseOver='this.style.color="<?php echo $settings['ca_msg_btn_hover_color']; ?>"' onMouseOut='this.style.color="<?php echo $settings['ca_msg_btn_text_color']; ?>"'><?php echo wp_kses_post($settings['ca_msg_btn_text']); ?></button>
                </a>

            </div>

        </div>

    </div>

</div>
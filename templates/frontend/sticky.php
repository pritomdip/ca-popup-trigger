<div id="ca-sticky-id" class="ca-sticky-overlay">
  	<div class="ca-sticky-popup" style="background:<?php echo $settings['ca_sticky_bg_color']; ?>;">
  		
  	<?php if( $settings['ca_sticky_cross_btn'] === 'show' ): ?>

  		<div class="ca-sticky-close"><div class="ca-sticky-close-btn">&times;</div></div>

  	<?php endif; ?>

		<div class="ca-sticky-content-wrapper" style="color:<?php echo $settings['ca_sticky_font_color']; ?>;">

		<?php if( !empty($settings['ca_sticky_title_1'])) : ?>
			<div class="ca-sticky-content">
				<?php echo _e('&#10003 ', 'ca-popup-trigger') . wp_kses_post($settings['ca_sticky_title_1']); ?>
			</div>
		<?php endif; ?>

		<?php if( !empty($settings['ca_sticky_title_2'])) : ?>
			<div class="ca-sticky-content">
					<?php echo _e('&#10003 ', 'ca-popup-trigger') . wp_kses_post($settings['ca_sticky_title_2']); ?>		
			</div>
		<?php endif; ?>

		<?php if( !empty($settings['ca_sticky_title_3'])) : ?>
			<div class="ca-sticky-content">
				<?php echo _e('&#10003 ', 'ca-popup-trigger') . wp_kses_post($settings['ca_sticky_title_3']); ?>									
			</div>
		<?php endif; ?>

		</div>

		<div class="ca-sticky-btn-wrapper">
	   		<a href="<?php echo esc_url($settings['ca_sticky_button_link']); ?>"><button class="ca-sticky-button" style="background:<?php echo $settings['ca_sticky_button_bg_clr']; ?>; color:<?php echo $settings['ca_sticky_button_text_clr']; ?>" onMouseOver='this.style.color="<?php echo $settings['ca_sticky_button_hover_clr']; ?>"' onMouseOut='this.style.color="<?php echo $settings['ca_sticky_button_text_clr']; ?>"'><?php echo wp_kses_post($settings['ca_sticky_button_text']); ?></button>
	   		</a>
		</div>

  	</div>
</div>
<div id="ca-sidebar-id" class="ca-sidebar-wrapper">
  	<div id='ca-sidebar-left-wrapper' class="ca-sidebar-left <?php echo $class; ?> ">

  		<div class='ca-sidebar-left-trigger' style="background:<?php echo $settings['ca_sidebar_title_bg_color']; ?>;">
  			<div class="ca-sidebar-title">
  				<div class="ca-title-wrapper">
					<div class="ca-triangle <?php echo $icon; ?>"></div>
				</div>
  				<div class="ca-title-wrapper ca-title-content-wrapper" style="color: <?php echo $settings['ca_sidebar_title_font_color']; ?>;">
  					<?php echo wp_kses_post( $settings['ca_sidebar_title'] ); ?>		
  				</div>	
  			</div>					     	
	    </div>

	    <div class='ca-sidebar-left-content' style="background:<?php echo $settings['ca_sidebar_bg_color']; ?>;">
	    	<div class="ca-content-wrapper">

	    		<div class="content-1" style="color:<?php echo $settings['ca_sidebar_content1_color']; ?>";><?php echo wp_kses_post( $settings['ca_sidebar_content1'] ); ?></div>

	    		<div class="content-2" style="color:<?php echo $settings['ca_sidebar_content2_color']; ?>";><?php echo wp_kses_post( $settings['ca_sidebar_content2'] ); ?></div>

	    		<div class="content-3" style="color:<?php echo $settings['ca_sidebar_content3_color']; ?>";><?php echo wp_kses_post( $settings['ca_sidebar_content3'] ); ?></div>

	    		<a href="<?php echo esc_url($settings['ca_sidebar_btn_link']); ?>">
	    			<button class="ca-sidebar-btn button-mb-20" style="background:<?php echo $settings['ca_sidebar_btn_bg_color']; ?>; color:<?php echo $settings['ca_sidebar_btn_text_color']; ?>;" onMouseOver='this.style.color="<?php echo $settings['ca_sidebar_btn_hover_color']; ?>"' onMouseOut='this.style.color="<?php echo $settings['ca_sidebar_btn_text_color']; ?>"'><?php echo wp_kses_post($settings['ca_sidebar_btn_text']); ?></button>
	    		</a>
	    	</div>
	    </div>

  	</div>
</div>
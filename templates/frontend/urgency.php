<section class="ca-urgency-popup-wrapper <?php echo $settings['ca_urgency_popup_position']; ?>">

    <div class="ca-urgency-notification first-urgency" id="first-urgency-trigger" style="background: <?php echo $settings['ca_urgency_content1_bg_color']; ?>" data-appear="<?php echo $settings['ca_urgency_content1_appearing_time']; ?>" data-hide="<?php echo $settings['urgency_content1_hiding_time']; ?>">

        <div class="ca-urgency-notification-container">

            <div class="ca-urgency-notification-image-wrapper">
                <p class="dashicons <?php echo $settings['ca_urgency_content1_icon']; ?>"></p>
            </div>

            <div class="ca-urgency-notification-content-wrapper">
                <p class="ca-urgency-notification-content" style="color: <?php echo $settings['ca_urgency_content1_font_color']; ?>"><?php echo wp_kses_post( $settings['ca_urgency_content1'] ); ?></p>
            </div>
        </div>

        <?php if( $settings['ca_urgency_cross_btn'] === 'show' ){ ?>
        <div class="ca-urgency-close"></div>
        <?php } ?>

    </div>

    <?php if( $settings['ca_urgency_content2_visible'] === 'yes' && !empty($settings['ca_urgency_content2']) ){ ?>

    <div class="ca-urgency-notification second-urgency" id="second-urgency-trigger" style="background: <?php echo $settings['ca_urgency_content2_bg_color']; ?>" data-appear="<?php echo $settings['ca_urgency_content2_appearing_time']; ?>" data-hide="<?php echo $settings['urgency_content2_hiding_time']; ?>">

        <div class="ca-urgency-notification-container">

            <div class="ca-urgency-notification-image-wrapper">
                <p class="dashicons <?php echo $settings['ca_urgency_content2_icon']; ?>"></p>
            </div>

            <div class="ca-urgency-notification-content-wrapper">
                <p class="ca-urgency-notification-content" style="color: <?php echo $settings['ca_urgency_content2_font_color']; ?>"><?php echo wp_kses_post( $settings['ca_urgency_content2'] ); ?></p>
            </div>
        </div>

        <?php if( $settings['ca_urgency_cross_btn'] === 'show' ){ ?>
        <div class="ca-urgency-close"></div>
        <?php } ?>

    </div>

    <?php } ?>

    <?php if( $settings['ca_urgency_content3_visible'] === 'yes' && !empty($settings['ca_urgency_content3']) ){ ?>

    <div class="ca-urgency-notification third-urgency" id="third-urgency-trigger" style="background: <?php echo $settings['ca_urgency_content3_bg_color']; ?>" data-appear="<?php echo $settings['ca_urgency_content3_appearing_time']; ?>" data-hide="<?php echo $settings['urgency_content3_hiding_time']; ?>">

        <div class="ca-urgency-notification-container">

            <div class="ca-urgency-notification-image-wrapper">
                <p class="dashicons <?php echo $settings['ca_urgency_content3_icon']; ?>"></p>
            </div>

            <div class="ca-urgency-notification-content-wrapper">
                <p class="ca-urgency-notification-content" style="color: <?php echo $settings['ca_urgency_content3_font_color']; ?>"><?php echo wp_kses_post( $settings['ca_urgency_content3'] ); ?></p>
            </div>
        </div>

        <?php if( $settings['ca_urgency_cross_btn'] === 'show' ){ ?>
        <div class="ca-urgency-close"></div>
        <?php } ?>

    </div>

    <?php } ?>

</section>

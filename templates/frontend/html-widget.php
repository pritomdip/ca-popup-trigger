<section class="ca-urgency-popup-wrapper <?php echo $settings['ca_widget_popup_position']; ?>">

	<?php if( !empty( $settings['ca_widget'] ) ){ ?>
		<div class="ca-widget-1" id="ca-first-widget" data-appear="<?php echo $settings['ca_widget1_appearing_time']; ?>" data-hide="<?php echo $settings['ca_widget1_hiding_time']; ?>">
			<?php echo $settings['ca_widget']; ?>
		</div>
	<?php } ?>

	<?php if( !empty( $settings['ca_widget2'] ) ){ ?>
		<div class="ca-widget-2" id="ca-second-widget" data-appear="<?php echo $settings['ca_widget2_appearing_time']; ?>" data-hide="<?php echo $settings['ca_widget2_hiding_time']; ?>">
			<?php echo $settings['ca_widget2']; ?>
		</div>
	<?php } ?>

</section>
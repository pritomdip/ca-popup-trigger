<div id="ca-delayedPopup" class="ca-delayedPopupWindow <?php echo $settings['ca_overlay_popup_position']; ?>" data-appear = "<?php echo $settings['ca_overlay_appearing_time']; ?>" data-hide="<?php echo $settings['ca_overlay_hiding_time']; ?>">

	<div class="ca-overlay-title-wrapper" style="background:<?php echo $settings['ca_overlay_title_bg_color']; ?>; color:<?php echo $settings['ca_overlay_title_font_color']; ?>;">

		<div class="ca-title <?php echo $settings['ca_overlay_title_position']; ?>"><?php echo wp_kses_post( $settings['ca_overlay_title'] ); ?></div>

		<?php if( $settings['ca_overlay_cross_btn'] === 'show' ): ?>

  		<div class="ca-overlay-close"><div class="ca-overlay-close-btn">&times;</div></div>

  		<?php endif; ?>
		
	</div>

	<div class="ca-overlay-full-wrapper" style="background:<?php echo $settings['ca_overlay_bg_color'];?>;">

		<?php if($settings['ca_overlay_img_visibility'] === 'yes'){ ?>

		<div class="ca-overlay-image-wrapper">
			<?php
            $src = ca_popup_trigger_get_image( $post_id, $settings['ca_overlay_img_visibility'], 'ca_overlay_image' );

            if( !empty( $src ) ){ 
            	echo '<img class="ca-msngr-image-wrapper" src="'. $src . '" alt="image">' ;
            } 
        echo '</div>';
    	} ?>

        <div class="ca-overlay-content-wrapper" style="color:<?php echo $settings['ca_overlay_content_font_color']; ?>;">

    	<?php if( $settings['ca_overlay_content_style'] === 'message_box' ){ ?>

            <div class="ca-overlay-msg-box">
                <div><?php echo esc_html($settings['ca_overlay_full_msg']); ?></div>
            </div>

        <?php } else { ?>

            <div class="ca-overlay-list-view">

                <?php if(!empty($settings['ca_overlay_body_heading'])){ ?>

                <h5 class="ca-msngr-title <?php echo $settings['ca_overlay_body_heading_position']; ?>" style="color: <?php echo $settings['ca_overlay_body_heading_color']; ?>"><?php echo wp_kses_post( $settings['ca_overlay_body_heading'] ); ?></h5>
                
                <?php } ?>

                <div class="ca-overlay-list-content-wrapper <?php echo $settings['ca_overlay_content_position']; ?>">

                    <?php if(!empty($settings['ca_overlay_content_one'])){ ?>

                    <div class="ca-content">
                        <span class="ca-checkmark"><?php _e('&#10003', 'ca-popup-trigger'); ?></span>
                        <span><?php echo wp_kses_post( $settings['ca_overlay_content_one'] ); ?></span>
                    </div>

                    <?php } ?>
                    <?php if(!empty($settings['ca_overlay_content_two'])){ ?>

                    <div class="ca-content">
                        <span class="ca-checkmark"><?php _e('&#10003', 'ca-popup-trigger'); ?></span>
                        <span><?php echo wp_kses_post( $settings['ca_overlay_content_two'] ); ?></span>
                    </div>

                    <?php } ?>
                    <?php if(!empty($settings['ca_overlay_content_three'])){ ?>
                    
                    <div class="ca-content">
                        <span class="ca-checkmark"><?php _e('&#10003', 'ca-popup-trigger'); ?></span>
                        <span><?php echo wp_kses_post( $settings['ca_overlay_content_three'] ); ?></span>
                    </div>

                    <?php } ?>

                </div>

            </div>

           <?php } ?> 

            <a href="<?php echo esc_url($settings['ca_overlay_btn_link']); ?>" class="ca-overlay-btn-link">
                <button class="ca-overlay-btn" style="background:<?php echo $settings['ca_overlay_btn_bg_color']; ?>; color:<?php echo $settings['ca_overlay_btn_text_color']; ?>;" onMouseOver='this.style.color="<?php echo $settings['ca_overlay_btn_hover_color']; ?>"' onMouseOut='this.style.color="<?php echo $settings['ca_overlay_btn_text_color']; ?>"'><?php echo wp_kses_post($settings['ca_overlay_btn_text']); ?></button>
            </a>

        </div>
		
	</div>

</div>